# Dynamic time warping
Python code to calculate a dynamic time warping alignment of the nucleoli movement of the most external nucleoli. 
Input data comes from tracked nucleoli bright-field.