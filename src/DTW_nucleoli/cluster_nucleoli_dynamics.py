# Python script to run dynamic time warping of trajectories
# Author: Antonio Politi, MPIBPC Goettingen, apoliti@mpibpc.mpg.de
# Date: Feb. 2021

import numpy
import matplotlib.pyplot as plt

from tslearn.clustering import TimeSeriesKMeans
from tslearn.datasets import CachedDatasets
from tslearn.preprocessing import TimeSeriesScalerMeanVariance, \
    TimeSeriesResampler, TimeSeriesScalerMinMax
from datetime import datetime
from tslearn.utils import to_time_series_dataset
from numpy import genfromtxt
import csv
import statistics


fname = '../../resources/DTW_nucleoli_data/flat_traj_20210128_span0.5'
my_data = genfromtxt(fname + '.csv',
                     delimiter=',', skip_header= 1, missing_values= 'NA')
#print(my_data[my_data[:,0]==1,1])

seed = 0
numpy.random.seed(seed)
data = []

# Append a dataset as in the list of
for idx in range(1,int(max(my_data[:,0])+1)):
    data.append(my_data[my_data[:,0]==idx, 1])

Xori = to_time_series_dataset(data)

seed = datetime.now()
numpy.random.seed()
#Xori = TimeSeriesResampler(sz=37).fit_transform(Xori)
X_train = numpy.copy(Xori)
for idx, xx in enumerate(Xori):
    X_train[idx] = xx - numpy.mean(xx)
#X_train = TimeSeriesScalerMeanVariance().fit_transform(Xori)
#X_train = TimeSeriesScalerMinMax().fit_transform(X_train[:102])


print(Xori[3])
# Soft-DTW-k-means
print("Soft-DTW k-means")
nr_clusters = 3

sdtw_km = TimeSeriesKMeans(n_clusters = nr_clusters,
                           metric="softdtw",
                           metric_params={"gamma": .1},
                           verbose=True,
                           random_state = 3,
                           n_init = 10
                        )
print(sdtw_km.random_state)
#random_state = 5, if you want reproducible initiation
# sdtw_km = TimeSeriesKMeans(n_clusters = nr_clusters,
#                             metric="dtw",
#                             verbose=True)

# create output

y_pred = sdtw_km.fit_predict(X_train)
#y_pred = sdtw_km.fit_transform(X_train)
print("inertia")
print(sdtw_km.inertia_)
numpy.savetxt(fname + '_nrclusters' + str(nr_clusters) + '_class.csv', y_pred, delimiter=",")


sz = X_train.shape[1]
plt.figure(1)
for yi in range(nr_clusters):
    plt.subplot(3, 3, 1 + yi)

    for xx in X_train[y_pred == yi]:
        plt.plot(xx.ravel(), "k-", alpha=.2)

    plt.plot(sdtw_km.cluster_centers_[yi].ravel(), "r-")
    plt.xlim(0, sz)
    #plt.ylim(-4, 4)
    plt.text(0.55, 0.85,'Cluster %d' % (yi + 1),
             transform=plt.gca().transAxes)
    if yi == 1:
        plt.title("Soft-DTW $k$-means data - mean")

plt.tight_layout()
plt.savefig(fname = fname +  '_nrclusters' + str(nr_clusters) + '_softDTW_1.png' )
plt.figure(2)

for yi in range(nr_clusters):

    plt.subplot(3, 3, 1 + yi)
    mean_overall = numpy.mean(Xori[y_pred==yi])
    for xx in Xori[y_pred == yi]:
        plt.plot(xx.ravel(), "k-", alpha=.2)
    plt.plot(sdtw_km.cluster_centers_[yi].ravel()+mean_overall, "r-")
    plt.xlim(0, sz)
    #plt.ylim(-4, 4)
    plt.text(0.55, 0.85,'Cluster %d' % (yi + 1),
             transform=plt.gca().transAxes)
    if yi == 1:
        plt.title("Soft-DTW $k$-means raw data")

numpy.savetxt(fname +  '_nrclusters' + str(nr_clusters) +'_barycenter.csv', numpy.concatenate(sdtw_km.cluster_centers_, axis = 1))

plt.tight_layout()
plt.savefig(fname = fname +  '_nrclusters' + str(nr_clusters) +'_softDTW_2.png' )
plt.show()
