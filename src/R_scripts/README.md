# R utility scripts
The scripts use the package *nucChromDistUtils*.
R scripts that are used to pre-process the image data, automatically create IJ-script to run the nuclei-polarity plugins 
etc.

## dnapolarity
Generate batch IJ-scripts to run plugin *nuclei polarity*. Collect the results of the matlab processing steps.

## nucleolidistribution
Scripts associated with the nucleoli movement measured from bright-field images and DTW analysis.

## rawdata_preparation
Some helper scripts to convert data in the appropriate format.