function outtable =  readIJpolarityCsvfile(csvfile)
if nargin < 1
    csvfile = 'Y:\schuh_lab\Tommaso on Microscope Facility\191202_data for paper\Process_20191210\time progression analysis\191002-cow44th-8003-10_directions.csv';
end

%%
outtable = readtable(csvfile, 'Delimiter', ',', 'NumHeaderLines', 1);
% convert all entries to double
for i = 2:size(outtable,2)
    outtable.(i) = outtable.(i);
end
% find absolute path 
[maindir, fname, ext] = fileparts(csvfile);

% derive time point an add a column
for i = 1:2:size(outtable,1)
    
    [apath, fname, ext] = fileparts(outtable.Filename{i});
    polname = [fname '_polarity.tif'];
    DTname = [fname '_DT.tif'];
    if exist(fullfile(maindir, polname)) 
        outtable.polfile{i} = polname;
        outtable.polfile{i+1} = polname; 
    end
    if exist(fullfile(maindir, DTname)) 
        outtable.DTfile{i} = DTname;
        outtable.DTfile{i+1} = DTname;
    end
    reg = regexp(outtable.Filename{i},'.*T(?<T>\d+).tif$', 'names');
    outtable.T(i) = str2num(reg.T);
    outtable.T(i+1) = str2num(reg.T);
end 

%% Assign nucleus 1 or 2 according to distances 
outtable.nucID(1) = 1;
outtable.nucID(2) = 2;
for i = 1:2:size(outtable,1)-2
    % find which is id 1 or 2
    id1 = i.*(outtable.nucID(i) == 1) + (i+1)*(outtable.nucID(i+1) == 1);
    id2 = i.*(outtable.nucID(i) == 2) + (i+1)*(outtable.nucID(i+1) == 2);
    d11 = distSphere(outtable, id1, i+2);
    d12 = distSphere(outtable, id1, i+3);
    
    if (d11 < d12)
        outtable.nucID(i+2) = 1;
        outtable.nucID(i+3) = 2;
    else
        outtable.nucID(i+2) = 2;
        outtable.nucID(i+3) = 1;
    end
end
sortrows(outtable, 'T');

end
function dist =  distSphere(intable, i, j)
    dist = sqrt( (intable.X_sphere(i) - intable.X_sphere(j))^2+(intable.Y_sphere(i) - intable.Y_sphere(j))^2 + ...
           (intable.Z_sphere(i) - intable.Z_sphere(j))^2);
end