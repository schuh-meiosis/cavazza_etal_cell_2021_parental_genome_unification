function [poi, mask, distT] = loaddata(pol, DT)
% load image data according to path
if nargin < 1
    testdir = 'T:\Antonio (apoliti)\Projects_Facility\CavazzaTommaso\tier1\Untreated\190406_zygote_cow15_8003_01\input_images_C02';
    [fname, path] = uigetfile(...
         '*.tif', 'Pick an image file', testdir);
    path = fullfile(path, fname);
end
%%
[bfreader, dim, tstamp] = getBioformatsReader(pol);
poi = getTPointBioFormats(bfreader, dim, 1, 1);
mask{1} = getTPointBioFormats(bfreader, dim, 1, 2);
mask{2} = getTPointBioFormats(bfreader, dim, 1, 3);
%%
[bfreader, dim, tstamp] = getBioformatsReader(DT);
distT{1} =  getTPointBioFormats(bfreader, dim, 1, 1);
distT{2} =   getTPointBioFormats(bfreader, dim, 1, 2);
end