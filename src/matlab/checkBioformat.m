
function status = checkBioformats()
% check for existence of bioformat package for MATLAB
status = 0;
if ~exist('bfCheckJavaPath','file')
    return;
end
[status, version] = bfCheckJavaPath();

if ~strcmp(version, '6.3.0')
    warning('You are not using bioformat version 6.3.0. Eventually install from install https://www.openmicroscopy.org/bio-formats/downloads/');
end
end