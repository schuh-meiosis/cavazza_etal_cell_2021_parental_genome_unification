function [header, outstr, logstr] = computePoiDistribution(csvfiles, varargin)
% csvfile is a comma separated file containing name of files and some
% post-processing data from sphere polarity ImagJ script
% It has following entries
% Units in pixels with XY pixel size as reference
% Filename	XY_um_px	Z_um_px	X_sphere	Y_sphere	Z_sphere	R_sphere	X_dirnuc	Y_dirnuc	Z_dirnu	X_dirmaxpol	Y_dirmaxpo	Z_dirmaxpo
% 190817_cow34_8002_3_C02_T90.tif	0.152119064	3	452.5468281	466.904003	229.8857161	86.4454437	-0.783798219	-0.412219804	0.464473019	-0.93910701	-0.035635764	0.341772023
% 190817_cow34_8002_3_C02_T90.tif	0.152119064	3	318.2244136	396.0844771	309.8428211	99.02769307	0.783798219	0.412219804	-0.464473019	0.3774811	0.713490739	-0.590287205

if ~checkBioformat
    % try to add the path
    addpath(genpath(userpath));
    if ~checkBioformat
        display('Could not find bioformat. Check the path or install https://www.openmicroscopy.org/bio-formats/downloads/');
        return;
    end
end
version = '0.1.4';
%% Default values for testing
global maindir_global;
if nargin < 1
    if maindir_global
        [csvfiles, fpath] = uigetfile({'*.csv';'*.*'},  'Specify csv file(s) generated from IJ nuclei polarity', ...
        maindir_global, 'MultiSelect', 'on');
        maindir_global = fpath;
        csvfiles = fullfile(maindir_global, csvfiles);
    else
        [csvfiles, fpath] = uigetfile({'*.csv';'*.*'}, 'Specify csv file(s) generated from IJ nuclei polarity', '.',...
            'MultiSelect', 'on');
        maindir_global = fpath;
        csvfiles = fullfile(maindir_global, csvfiles);
    end
    if ~iscell(csvfiles)
        csvfiles = {csvfiles};
    end
end


%%
for csvfile = csvfiles
    poiDistribution(csvfile{1}, version, varargin{:});
end
end

function poiDistribution(csvfile, version, varargin )
%% Parameter input
defaultReportdir = '';
defaultMedian = 3; % [3 3] correpsond to 1 filter in IJ1
defaultGauss = 1.0;
defaultTol = 0.1;
p = inputParser;
addOptional(p, 'gauss', defaultGauss, @isnumeric);
addOptional(p, 'median', defaultMedian, @isnumeric);
addOptional(p, 'tol', defaultTol, @isnumeric);
addRequired(p, 'csvfile', @(x) exist(x, 'file'));
parse(p, csvfile, varargin{:});
p = p.Results;
p.median = [p.median p.median];
fclose('all');
intable = readIJpolarityCsvfile(p.csvfile);
[path, name, ext] = fileparts(p.csvfile);
%%


valNuc = {};
outfname = fullfile(path, [name '_poiDist' datestr(date, 'yyyymmdd') '.txt']);
logfname = fullfile(path, [name '_poiDist' datestr(date, 'yyyymmdd') '.log']);
flog = fopen(logfname, 'w');
fclose(flog);
if (exist(outfname, 'file'))
    delete(outfname);
end

for ifile = [size(intable,1)-1:-2:1]
    logstr = '';
    segID = {[1:3], [4:6]};
    polfile = fullfile(path, intable.polfile{ifile});
    DTfile = fullfile(path, intable.DTfile{ifile});
    if ~(exist(polfile, 'file')==2) || ~(exist(DTfile, 'file')==2)
        continue;
    end
    [poi, mask, distT] = loaddata(polfile, DTfile);
    if (intable.nucID(ifile) == 2)
        segID = {segID{2}, segID{1}};
        distT = {distT{2}, distT{1}};
    end
    
    % reassign sphere ids if necessary
    
    
    %QC of sphere given volume inside image
    for i = 0:1
        sphdim = [intable.X_sphere(ifile+i), intable.Y_sphere(ifile+i), intable.Z_sphere(ifile+1), intable.R_sphere(ifile+1)];
        intable.QC(ifile+i) = QCsphere(sphdim, size(mask{1}), intable.Z_um_px(ifile+1)/intable.XY_um_px(ifile+1));
        for seg = segID{i+1}
            if ~any(mask{1}(:), seg)
                intable.QC(ifile+i) = 0;
            end
        end
    end
    
    % check if it contains at least all segments
    
    fprintf('%s %d %s\n', 'Process fileId', ifile, name);
    logstr = sprintf('%s%s %d %s\n', logstr, 'Process fileId', ifile, name);

    % reference slice is the last one in the series (if this is the last
    % time point !) this has been reordered in the table
    poif = poi;
    if (~isempty(p.median))
        for iz = 1:size(poif,3)
            poif(:,:,iz) = medfilt2(poif(:,:,iz), p.median);
        end
    end
    
    if (p.gauss > 0)
        for iz = 1:size(poif,3)
            poif(:,:,iz) = imgaussfilt(poif(:,:,iz), p.gauss);
        end
    end
    poinuc = zeros(size(poif,1), size(poif,2), size(poif,3), 2, 'uint8');
    
    
    for inuc = 1:2
        mask_nuc = ismember(mask{1}, segID{inuc});
        idx = find(mask_nuc(:) == 1);
        % find best threshold
        
        [L(ifile, inuc), logstr_inuc] = findThreshold(poif, idx, valNuc, ifile, inuc, p.tol/2);
        logstr = sprintf("%s%s", logstr, logstr_inuc);  
        valNuc{ifile, inuc} = computeInt(poif, idx, L(ifile, inuc));
        
        
        polNuc_dirnuc = computePolarity(poif, mask{1}, segID{inuc}, ...
            distT{inuc}, valNuc{ifile, inuc}.thr, valNuc{ifile, inuc}.bg);
        polNuc_dirmax = computePolarity(poif, mask{2}, segID{inuc}, ...
            distT{inuc}, valNuc{ifile, inuc}.thr, valNuc{ifile, inuc}.bg);
        polfields = fieldnames(polNuc_dirnuc);
        for ifield = 1:3
            valNuc{ifile, inuc} = setfield(valNuc{ifile, inuc}, ...
            ['dirnuc_' polfields{ifield}], polNuc_dirnuc.(polfields{ifield}));
        end
        for ifield = 1:3
            valNuc{ifile, inuc} = setfield(valNuc{ifile, inuc}, ...
            ['dirmax_' polfields{ifield}], polNuc_dirmax.(polfields{ifield}));
        end
        for ifield = 4:length(polfields)
            valNuc{ifile, inuc} = setfield(valNuc{ifile, inuc}, ...
            [polfields{ifield}], polNuc_dirmax.(polfields{ifield}));
        end
        poinuc(:,:,:,inuc) = uint8(poif.*mask_nuc.*(poif>L(ifile,inuc)));
        
    end
    createOutputLinebyLine(intable(ifile,:), valNuc{ifile,1}, outfname,  version)
    createOutputLinebyLine(intable(ifile+1,:), valNuc{ifile,2}, outfname,  version)
    flog = fopen(logfname, 'a');
    fprintf(flog, '%s', logstr);
    fclose(flog);
    %createOutputLinebyLine(intable(ifile,:), valNuc{ifile,1}, outfname,  version)
    % this is the segmented mask that has been also processed with a median
    % filter and a gaussian filter
    [path_segmask, filename_segmask, ext] = fileparts(polfile);
    saveSegmentMask(poinuc, fullfile(path_segmask, [strrep(filename_segmask, 'polarity',  'POIthr') '.tif']));
end
end
function ratioVol =  QCsphere(sphdim, imgdim, Z_XY_ratio)
%%
scaling = [1,1,Z_XY_ratio];
Vsph = sphdim(4)^3*pi*4/3;
Vsec = 0;
for idim = 1:3
    h = (sphdim(idim)- sphdim(4));
    if (h < 0)
        Vsec = Vsec + h^2*pi/3*(3*sphdim(4)- abs(h));
    end
    h = imgdim(idim)*scaling(idim) - (sphdim(idim) +  sphdim(4));
    if (h < 0)
        Vsec = Vsec + h^2*pi/3*(3*sphdim(4)- abs(h));
    end
end
ratioVol = (Vsph-Vsec)/Vsph*(Vsph>Vsec) + 0*(Vsph < Vsec);
end

function [Lm, logstr] = findThreshold(poif, idx, valNuc, ifile, inuc, loctol)
logstr = '';
thr = multithresh(poif(idx), 1);
Lm = thr(1);

valNuc{ifile, inuc}  = computeInt(poif, idx, Lm);
factInt = valNuc{end, inuc}.totint/valNuc{ifile, inuc}.totint;
refIntThr =  valNuc{end, inuc}.totintThr;
ratiom = valNuc{ifile, inuc}.totintThr/refIntThr*factInt;
nrsteps = 0;

% eventually find best threshold to conserve total intensity
Lh = 4*Lm;
Ll = 0;
fprintf('%s ', ['Optimize threshold nuc ' num2str(inuc)]);
logstr = sprintf('%s%s ', logstr, ['Optimize threshold nuc ' num2str(inuc)]);

while(abs(ratiom - 1) > loctol )
    logstr = sprintf('%s%s', logstr, '.');
    fprintf('%s', '.');
    nrsteps = nrsteps+1;
    inth = computeInt(poif, idx, Lh);
    intl = computeInt(poif, idx, Ll);
    intm = computeInt(poif, idx, Lm);
    ratioh  = inth.totintThr/refIntThr*factInt;
    ratiol = intl.totintThr/refIntThr*factInt;
    if ~(ratiol > 1 && ratioh < 1)
        fprintf('%s\n', 'NO CONVERGENCE total intensity');
        logstr = sprintf('%s%s\n', logstr, 'NO CONVERGENCE total intensity');
        break
    end
    ratiom = intm.totintThr/refIntThr*factInt;
    if (ratiom < 1 - loctol)
        Lh = Lm;
    end
    if (ratiom > 1 + loctol)
        Ll = Lm;
    end
    Lm = (Ll + Lh)/2;
    if nrsteps == 100
        loctol = loctol*1.1
    end
    if abs(Lh-Ll) < 1e-3
        fprintf('%s\n','NO CONVERGENCE exact point not found');
        logstr = sprintf('%s%s\n', logstr, 'NO CONVERGENCE total intensity');
        break
    end
end
fprintf('\n');
logstr = sprintf('%s\n', logstr);
end

function saveSegmentMask(img, fname)
% save an image stored in data stack img, with name fname and in reportDir
% ome saving is very slow in fact one should find a better way save with
% metadata
% only for debug and report


%metadata = createMinimalOMEXMLMetadata(img);
%pixelSize = ome.units.quantity.Length(java.lang.Double(0.1479), ome.units.UNITS.MICROMETER);
%metadata.setPixelsPhysicalSizeX(pixelSize, 0);
%metadata.setPixelsPhysicalSizeY(pixelSize, 0);
%pixelSizeZ = ome.units.quantity.Length(java.lang.Double(2.5), ome.units.UNITS.MICROMETER);
%metadata.setPixelsPhysicalSizeZ(pixelSizeZ, 0);
for inuc = 1:size(img,4)
    for iz = 1:size(img,3)
        if inuc == 1 && iz == 1
            imwrite(img(:,:,iz, inuc), fname );
        else
            imwrite(img(:,:,iz, inuc), fname, 'writemode', 'append');
        end
    end
end
end

function  createOutputLinebyLine(inrow, val, outfname,  version)
outstr = '';
inrow_names = fieldnames(inrow);
parnames = fieldnames(val);
if (~exist(outfname, 'file'))
    fid = fopen(outfname, 'w');
    header = sprintf('computePoiDistribution %s. Coordinates in pixels with XY pixel size as reference\n', version);
    for ifield = 1:length(inrow_names) - 3
       header = sprintf('%s%s\t', header, inrow_names{ifield});
    end
    for ifield = 1:length(parnames)

        vals = val.(parnames{ifield});
        if length(vals) == 1 
            if ifield == length(parnames)
                header = sprintf('%s%s', header, parnames{ifield});
            else    
                header = sprintf('%s%s\t', header, parnames{ifield});
            end
        else
            for i = 1:length(vals)
                if i == length(vals) && ifield == length(parnames)
                    header = sprintf('%s%s', header, [parnames{ifield} '_' num2str(i)]);
                else
                    header = sprintf('%s%s\t', header, [parnames{ifield} '_' num2str(i)]);
                end
            end
        end
    end
    header = sprintf('%s\n', header);
    fprintf(fid, '%s', header);
else
    fid = fopen(outfname, 'a');
end
%%
outstr = sprintf('%s', inrow.Filename{1});
inrow_names = fieldnames(inrow);
for i=2:size(inrow,2)
    if strcmp(inrow_names{i}, 'nucID') || strcmp(inrow_names{i}, 'T') 
       outstr = sprintf('%s\t%d',outstr, inrow.(i));
       continue;
    end
    if strcmp(inrow_names{i}, 'polfile') || strcmp(inrow_names{i}, 'DTfile')
       outstr = sprintf('%s\t%s',outstr, inrow.(i){1});
       continue;
    end
    outstr = sprintf('%s\t%f', outstr, inrow.(i));
end

for i = 1:size(parnames)
    vals = val.(parnames{i});
    for j = 1:length(vals)
        outstr = sprintf('%s\t%f', outstr, vals(j));
    end
end


fprintf(fid, '%s\n', outstr);
fclose(fid);
end

function out = computePolarity(poif, mask,  levels, distT, L, bg)
% compute intensity within the 3 ranges subtracted by the bg
% of threshold mask background subtracted and non-thresholded
out = struct();
for l = 1:length(levels)
    idx = find(mask == levels(l));
    idx_thr = idx(poif(idx) >= L);
    % this gets also below 0
    % out.pol(l) = mean(poif(idx)) - bg;
    out.section(l) = mean(poif(idx));
    out.section_thr(l) = sum(poif(idx_thr))/length(idx);
    out.section_thrbgsub(l) = (sum(poif(idx_thr)) - bg.*length(idx_thr))/length(idx);
    
    if out.section_thrbgsub(l) < 0
        out.pol_thrbgsub(l) = 0;
    end
end
% find index of distT
medianDist = quantile(distT(distT(:)>0), 0.5);
radBorder = [0; medianDist; max(distT(:))];

for i = 1:length(radBorder)-1
    mask_dist = logical((distT > radBorder(i)).*(distT <= radBorder(i+1)));
    idx_dist = find(mask_dist);
    % Using threshold
    idx_dist_thr = idx_dist(poif(idx_dist) >= L);
    out.radial(i) = mean(poif(idx_dist));
    out.radial_thr(i) = sum(poif(idx_dist_thr))/length(idx_dist);
    out.radial_thrbgsub(i) = (sum(poif(idx_dist_thr)) - bg.*length(idx_dist_thr))/length(idx_dist);
    % alternative is out.rad(i) = (mean(poif(idx_dist)) - bg);
    % for consistency use previous measure
end

end

function out = computeInt(poif, idx, L, bgref)
% compute total intensity of thresholded area
% compute bg value
% ratio of number of pixels

idx_thr = idx(poif(idx) >= L);
idx_bg = idx(poif(idx) < L);
out = struct();

out.totint = sum(poif(idx));
out.totnrpx = length(idx);
out.thr  = L;
if(isempty(idx_bg))
    out.bg = 0;
else
    out.bg = mean(poif(idx_bg));
end
if nargin == 4
    if bgref > 0
        out.bg = bgref;
    end
end

out.totintThr = sum(poif(idx_thr)) - out.bg*length(idx_thr);
out.totnrpxThr =  length(idx_thr);
end