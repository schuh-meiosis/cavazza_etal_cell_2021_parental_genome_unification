function stack = getTPointBioFormats(reader,  dim, iT, iC)
%%
% getTPointBioFormats(reader, tpoint, dim, Ch)
%   return a matlab Z-stack from a multitif image
%       reader: bioformat reader from getBioFormatReader and bfGetReader(filepath)
%       iT: time point to use, integer
%       dim:    dimension obtained from getBioFormatReader. dim = struct('Nx', Nx, 'Ny', Ny, 'Nz', Nz, 'Nc', Nc, 'Nt', Nt, 'voxelSize', voxelSize);
%       iCh:     Channel number an intenger

%  Antonio Politi, EMBL, November 2016
%
  
    % stack  = zeros(dim.Nx, dim.Ny, dim.Nz);
    % this is the correct size for matlab as it is inverted XY -> YX
    stack  = zeros(dim.Ny, dim.Nx, dim.Nz);
    assert(strcmp(reader.getDimensionOrder, 'XYCZT') || strcmp(reader.getDimensionOrder, 'XYZCT'), 'Only reads XYCZT or XYZCT format for the moment')
    try
        for iZ = 1:dim.Nz
            iPlane = reader.getIndex(iZ-1,iC-1,iT-1) + 1;
            stack(:,:,iZ) = bfGetPlane(reader, iPlane);
        end
    catch
        for iZ = 1:dim.Nz
            iPlane = reader.getIndex(iZ-1,iC-1,iT-1) + 1;
            stack(:,:,iZ) = bfGetPlane(reader, iPlane)';
        end
    end
end