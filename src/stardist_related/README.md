# Segmentation and tracking of nuclei and nucleoli
Scripts used to segment and track nucleoli and nuclei from bright-field images. 
For the segmentation models trained using [stardist](https://github.com/stardist/stardist) have been useds. The final
models can be found in resources. The modified code is in the tar.gz file. 

  