@ File (label = "imagefile") afile
@ File (label="prediction directory", style="directory") roidir
@ File (label="output qc data", style="directory") outdir

# Tool to combine prediction roi and image so that user can cleanup the prediction
# To automatically save the roi press the key "g" when the image is highlighted

import os
from ij import IJ, WindowManager
from ij.plugin.frame import RoiManager
from java.awt.event import KeyEvent, KeyAdapter
from ij.gui import YesNoCancelDialog

def doSomething(imp, keyEvent):
  """ A function to react to key being pressed on an image canvas. """
  IJ.log("clicked keyCode " + str(keyEvent.getKeyCode()) + " on image " + str(imp))
  # Prevent further propagation of the key event:
  #type a g to save the roi
  idx = 0
  if keyEvent.getKeyCode() == 71:
    outfile= os.path.join(outdir.getPath(), imp.getTitle() + ".zip")
    if os.path.exists(outfile):
        msg = YesNoCancelDialog(IJ.getInstance(), "Manual qc Predictions", "Do you want to overwrite " + outfile +" ?")
        if not msg.yesPressed():
            while os.path.exists(outfile):
                idx = idx+1
                outfile = os.path.join(outdir.getPath(), imp.getTitle() + "_" + str(idx) + ".zip")
    IJ.log("Saving the ROIs")
    rm = RoiManager()
    rm = rm.getInstance()
    rm.setSelectedIndexes(range(0, rm.getCount()))
    rm.runCommand("Save",  outfile)
    IJ.showMessage("ROIs have been saved to " + outfile )
  keyEvent.consume()

class ListenToKey(KeyAdapter):
  def keyPressed(this, event):
    imp = event.getSource().getImage()
    doSomething(imp, event)

#name of folder that contains predictions of nucleoli after cleanup using nuclei-mask has been done
#roidir = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round2/qc/nucleoli_cleanup"
#roidir = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round2/qc/nuclei"

#tifdir = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round2/qc/tifs"

def main():
    IJ.run("Close All")
    
    fname = os.path.splitext(os.path.basename(afile.getPath()))[0]
    zipinfile = os.path.join(roidir.getPath(), fname + ".zip")
    zipoutfile = os.path.join(outdir.getPath(), fname + ".zip")
    if not os.path.exists(zipinfile):
        IJ.showMessage("Exiting macro. Could not find file " + zipinfile)
        return
    if os.path.exists(zipoutfile):
        msg = YesNoCancelDialog(IJ.getInstance(), "Manual qc Predictions", zipoutfile +"\nOutput file exists do you want to continue?")
        if not msg.yesPressed():
            return
    fpath = os.path.dirname(afile.getPath())
    imp = IJ.openImage(afile.getPath())
    imp.show()
    imp2 = imp.duplicate()
    imp2.show()
    IJ.run("Combine...", "stack1="+imp.getTitle()+" stack2="+imp2.getTitle()+"")
    imp = IJ.getImage()
    imp.setTitle(fname)
    rm = RoiManager()
    rm = rm.getInstance()
    rm.reset()
    
    rm.runCommand("Open", zipinfile)
    IJ.run("Enhance Contrast", "saturated=0.35")
    
    listener = ListenToKey()
    win = imp.getWindow()
    canvas = win.getCanvas()
     # Remove existing key listeners
    kls = canvas.getKeyListeners()
    map(canvas.removeKeyListener, kls)
    # Add our key listener
    
    canvas.addKeyListener(listener)
    print(kls)
    map(canvas.addKeyListener, kls)

main()