# Utility script to copy files 
import os
import shutil

indir_nucleoli = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/predictions/nucleoli_0.46_0.3/"
indir_nuclei = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/predictions/nuclei_0.4_0.8/"

outdir_nuclei = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/qc/nuclei_0.4_0.8/"
outdir_nucleoli = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/qc/nucleoli_0.46_0.4/"


files = os.listdir("Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/qc/tifs")

for afile in files:
	name = os.path.splitext(afile)[0]
	
	shutil.copyfile(indir_nucleoli + name + ".zip", outdir_nucleoli  + name + ".zip")
	shutil.copyfile(indir_nuclei + name + ".zip", outdir_nuclei  + name + ".zip")