@ File[] myFiles
@ File (style="directories") roidir

# Remove nucleoli that are not within a nucleus
# Read 
from ij.plugin.frame import RoiManager
from ij import IJ, ImagePlus
import os

frac = 0.1 # the fraction of nucleoli that is in the nuclei
#roidir = "../ML_Predictions_round4/qc/"
for myFile in myFiles:
	IJ.run("Close All")
	imp = IJ.openImage(myFile.getPath())
	imp.show()
	name_noext = os.path.splitext(myFile.getName())[0]
	rm_nuclei = RoiManager(1)
	rm_nuclei.reset()
	
	rm_nucleoli = RoiManager()
	rm_nucleoli = rm_nucleoli.getInstance()
	
	rm_nucleoli.reset()
	if not os.path.exists(roidir + "nuclei_qc/"+ name_noext +".zip"):
		continue
	rm_nuclei.runCommand("Open", roidir + "nuclei_qc/"+ name_noext +".zip")
	rm_nucleoli.runCommand("Open", roidir + "nucleoli/"+ name_noext +".zip")
	
	
	rois_nuclei = rm_nuclei.getRoisAsArray()
	rois_nucleoli = rm_nucleoli.getRoisAsArray()
	Nframes = imp.getNFrames()
	#rm_nuclei.reset()
	#rm_nucleoli.reset()
	for i in range(0, Nframes):
		#rm_nuclei.reset()
		rois_nucleoli_frame = [idx for idx, roi in enumerate(rois_nucleoli) if roi.getZPosition() == i+1]
		rois_nuclei_frame = [idx for idx, roi in enumerate(rois_nuclei) if roi.getZPosition() == i+1]
		print(rois_nucleoli_frame)
		print(rois_nuclei_frame)
		
		if len(rois_nuclei_frame) == 0 or len(rois_nucleoli_frame) == 0:
			continue
		rm_nuclei.setSelectedIndexes(rois_nuclei_frame)
		
		if len(rois_nuclei_frame) > 1:
			rm_nuclei.runCommand(imp, "OR")
		else:
			imp.setRoi(rois_nuclei[rois_nuclei_frame[0]])
		mask = ImagePlus("mask", imp.createRoiMask())
		#mask.show()
		idx_remove = list()
		for idx in rois_nucleoli_frame:
			mask.setRoi(rois_nucleoli[idx])
			imgstat = mask.getStatistics()
			# remove rois that are below frac
			if imgstat.mean*imgstat.area < frac*imgstat.area*255:
				idx_remove.append(idx)
		rois_nucleoli = [elem for idx, elem in enumerate(rois_nucleoli) if idx not in idx_remove]
			
	
	rm_nucleoli.reset()
	[rm_nucleoli.addRoi(roi) for roi in rois_nucleoli]
	rm_nucleoli.setSelectedIndexes(range(0, len(rois_nucleoli)))
	rm_nucleoli.runCommand("Save", roidir + "nucleoli_cleanup/"+ name_noext +".zip")