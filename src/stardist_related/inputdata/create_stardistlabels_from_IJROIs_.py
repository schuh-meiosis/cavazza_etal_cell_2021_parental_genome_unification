#create a label mask compatible with stardist from rois. Copy labels to outdir/masks and images to outdir/images

import os
from ij import IJ
from ij.plugin.frame import RoiManager

roidir = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/qc/nuclei_qc"
#roidir = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/qc/nucleoli_qc"

tifdir = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/qc/tifs"
outdir = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/ML_Predictions_round3/ground_truth_update_nuclei"


files = os.listdir(roidir)
print(files)
for afile in files:
	IJ.run("Close All")
	afile_noext = os.path.splitext(afile)[0] 
	rm_rois = RoiManager()
	rm_rois = RoiManager.getInstance()
	rm_rois.reset()
	rm_rois.runCommand("Open", os.path.join(roidir, afile_noext +".zip"))
	
	rois = rm_rois.getRoisAsArray()
	rm_labels = RoiManager(1)
	rm_labels.reset()
	imp = IJ.openImage(os.path.join(tifdir, afile_noext + ".tif"))
	Nframes = imp.getNFrames()
	name = imp.getTitle()
	result = IJ.createImage(name + "labels", "8-bit black", imp.getWidth(), imp.getHeight(), Nframes-1)
			
	for i in range(0,Nframes-1):
			print i
			rois_frame = [roi for roi in rois if roi.getZPosition() == i+1]
			if len(rois_frame) == 0:
				continue
			result.setZ(i+1)
			ip = result.getProcessor()
			rm_labels.reset()
			[rm_labels.addRoi(roi) for roi in rois_frame]
			for index, roi in enumerate(rm_labels.getRoisAsArray()):
				ip.setColor(index+1)
				ip.fill(roi)
			ip.resetMinAndMax()
			result.setProcessor(ip)
			IJ.run(result, "glasbey", "")
	imp2 = imp.crop("1-"+str(Nframes-1))
	IJ.run(result, 
	"Image Sequence... ", 
	"format=TIFF name="+afile_noext+"_t digits=3 save=["+os.path.join(outdir, "masks", afile_noext+"_t000.tif")+"]");
	IJ.run(imp2, 
	"Image Sequence... ", 
	"format=TIFF name="+afile_noext+"_t digits=3 save=["+os.path.join(outdir, "images", afile_noext+"_t000.tif")+"]");
	
