# Create image that contains 2 time points corresponding to where nucleoli are visible
import csv
import os
from ij import IJ
from ij.plugin import Duplicator
from ij.plugin import HyperStackConverter
from ij.plugin import Concatenator
maindir = u'Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/videos_20210201/2021_cropped'

outdir = u'Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/videos_20210201/2021_cropped_2tpts'
filereader = csv.DictReader(open(u'Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/all_videos_v210201.csv'), delimiter = ",")
IJ.run("Close All")
IJ.resetEscape()
dp = Duplicator()
cc = Concatenator()
for row in filereader:
	name = os.path.splitext(row["file_name"])[0] + ".tif"
	fin_name = os.path.join(maindir, name)
	fout_name =  os.path.join(outdir, name)
	#if not row["clearframe_tier2"] == "yes":
	#	continue
	try:
		firstpt = int(row["X1st_clear_frame"])
	except:
		continue
	try:
		lastpt = int(row["Last_clear_frame"])
	except:
		continue
	if IJ.escapePressed():
		break
	print(os.path.exists(fin_name))
	if os.path.exists(fin_name) & ~os.path.exists(fout_name):
		imp = IJ.openImage(fin_name)
		imp2 = dp.run(imp, firstpt,firstpt)
		imp3 = dp.run(imp, lastpt,lastpt)
		impC = cc.concatenate(imp2, imp3, False);
		
		#imp3 = HyperStackConverter.toHyperStack(imp2, 1,1, int(endpoint))
		IJ.saveAs(impC, "Tiff", fout_name)
		imp.close()
		imp2.close()
		imp3.close()
		impC.close()