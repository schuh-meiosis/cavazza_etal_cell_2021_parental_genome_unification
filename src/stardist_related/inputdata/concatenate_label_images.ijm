@ File[]  (label = "select tif files", style = "file") tiffiles
@ File (label= "dir label" , style = "directory") dir_label
@ File (label= "dir_out", style = "directory") dir_out
// 2 time point label images in one image 
setBatchMode("hide");
for (i=0; i < tiffiles.length; i++) {
	run("Close All");
	myfile = tiffiles[i];
	print(myfile);
	dir_tiff = File.getDirectory(myfile);
	fname = File.getName(myfile);
	fname = File.getNameWithoutExtension(fname);

	dir_label = replace(dir_label, "\\", "/");

	run("Image Sequence...", "open=["+ dir_label +"]  file=(^"+ fname + "_.*) sort");
	run("Properties...", "channels=1 slices=1 frames=2 pixel_width=1.0000 pixel_height=1.0000 voxel_depth=1.0000");
	saveAs("Tiff", dir_out + "/" + fname + ".tif");
}
