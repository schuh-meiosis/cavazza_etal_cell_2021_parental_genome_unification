@ File[]  listOfFiles (label = "select files", style = "file")
for (i=0; i < listOfFiles.length; i++) {
	myFile=listOfFiles[i];
	open(myFile);
	setThreshold(1, 3);
	run("Convert to Mask");
	workdir = File.getDirectory(myFile);
	fname = File.getName(myFile);
	saveAs("Tiff", workdir + "/masks_joined/" + fname);
	run("Watershed");
	run("Connected Components Labeling", "connectivity=4 type=[8 bits]");
	saveAs("Tiff", workdir + "/masks_watershed/" + fname);
	run("Close All");
}

