@ File[] (label = "imagefiles", style = "file") myFiles
@ File (label = "nuclei roi and tracks", style = "directory") dir_nuc
@ File (label = "nucleoli roi", style = "directory") dir_nucleoli
@ File (label = "output nuclei rotated roi", style = "directory") dir_out_nuc
@ File (label = "output nucleoli rotated roi", style = "directory") dir_out_nucleoli
@ File (label = "output rotated image", style = "directory") dir_out_img





# Rotate and register image and rois so that image is always along
# nuclei and centered at the interface
# This script will rotate the ROIs in nuclei and nucleoli. Path are hardcoded!

import os
from ij import IJ, WindowManager
from ij.plugin.frame import RoiManager
from java.awt.event import KeyEvent, KeyAdapter
from ij.gui import YesNoCancelDialog
from java.io import File
import csv
import math

def rotate_rois(imp, roifile, delta_center, alpha):
	"""
		Rotate Rois
	"""
	rm = RoiManager()
	rm = rm.getInstance()
	rm.reset()
	rm.runCommand("Open", roifile)
	rm.runCommand(imp,"Show None")
	for iroi in range(0, rm.getCount()):
		rm.select(iroi)
		roi = rm.getRoi(iroi)
		slice = roi.getPosition()
		rm.translate(delta_center[slice-1][0], delta_center[slice-1][1])
		rm.deselect()
		rm.select(iroi)
		IJ.run(imp, "Rotate...", "rotate angle="+str(alpha[slice-1]))
		rm.runCommand(imp,"Update")
	
def compute_rotations(infile, NFrames, width, height):
	""" Read a tab delimited file that contains following columns XM, YM, Slice and TrackN
		of two tracked object per slice
		Compute rotation angle (degrees) and translation vector 
	"""
	csv_file = open(infile, 'r')
	dict_reader = csv.DictReader(csv_file, delimiter = '\t')
	csv_dict = list(dict_reader)
	delta_center = [[0,0]]*NFrames 
	alpha = [0]*(NFrames)
	print(alpha)
	startFrame = int(csv_dict[0]["Slice"])
	
	for iframe in range(startFrame, NFrames+1):
		nuc0	= [[float(pos["XM"]), float(pos["YM"])] for pos in csv_dict if pos["Slice"] == str(iframe) and pos["TrackN"] == '0'][0]
		nuc1	= [[float(pos["XM"]), float(pos["YM"])]  for pos in csv_dict if pos["Slice"] == str(iframe)  and pos["TrackN"] == '1'][0]
		nuc_vec = [nuc1[0]-nuc0[0], nuc1[1]-nuc0[1]]
		nuc_vec_norm = math.sqrt(nuc_vec[0]*nuc_vec[0]+nuc_vec[1]*nuc_vec[1])
		nuc_vec = [nuc_vec[0]/nuc_vec_norm, nuc_vec[1]/nuc_vec_norm]
		alpha[iframe-1] = 90+math.atan2(nuc_vec[0], nuc_vec[1])/math.pi*180
		delta_center[iframe-1] = [width/2 - (nuc0[0]+nuc1[0])/2, height/2 - (nuc0[1]+nuc1[1])/2]	
	return (delta_center, alpha)

def rotate_imp(imp, NFrames,  delta_center, alpha):
	for iframe in range(1, NFrames+1):
		imp.setT(iframe)
		IJ.run(imp, "Translate...", "x="+str(delta_center[iframe-1][0])+" y="+str(delta_center[iframe-1][1])+" interpolation=None slice");
		IJ.run(imp, "Rotate... ", "angle="+str(alpha[iframe-1])+"  grid=1 interpolation=Bilinear slice");
	return imp

for afile in myFiles:
	IJ.run("Close All")
	fname = os.path.splitext(os.path.basename(afile.getPath()))[0]

	outtif = os.path.join(dir_out_img.getPath(),  fname + ".tif")
	trackfile_nuclei = os.path.join(dir_nuc.getPath(), fname + ".txt")
	roifile_nuclei = os.path.join(dir_nuc.getPath(), fname + ".zip")
	roifile_nuclei_out = os.path.join(dir_out_nuc.getPath(), fname + ".zip")
	roifile_nucleoli = os.path.join(dir_nucleoli.getPath(),  fname + ".zip")
	roifile_nucleoli_out = os.path.join(dir_out_nucleoli.getPath(),  fname + ".zip")

	
	imp = IJ.openImage(afile.getPath())
	imp.show()
	IJ.run("Enhance Contrast", "saturated=0.35")
	(delta_center, alpha) = compute_rotations(trackfile_nuclei, imp.getNFrames(), imp.getWidth(), imp.getHeight())
	print(delta_center)
	print(alpha)
	imp = rotate_imp(imp, imp.getNFrames(), delta_center, alpha)
	IJ.saveAs(imp, "Tiff", outtif)
	rotate_rois(imp, roifile_nuclei, delta_center, alpha)
	rm = RoiManager()
	rm = rm.getInstance()
	rm.setSelectedIndexes(range(0, rm.getCount()))
	rm.runCommand("Save", roifile_nuclei_out)
	rotate_rois(imp, roifile_nucleoli, delta_center, alpha)
	rm.setSelectedIndexes(range(0, rm.getCount()))
	rm.runCommand("Save", roifile_nucleoli_out)
#	imp.setT(iframe)
#	IJ.run(imp, "Translate...", "x="+str(delta_center[iframe-1][0])+" y="+str(delta_center[iframe-1][1])+" interpolation=None slice");
#	IJ.run(imp, "Rotate... ", "angle="+str(alpha[iframe-1])+"  grid=1 interpolation=Bilinear slice");


#
#IJ.run(imp. "Tiff", os.path.join(outdir.getPath(), fname + ".tif")


