#@ File[] (style = "file") fnames
//Crop around zygote. Method is not very precise and requires manual inspection
for (i=0; i < fnames.length; i++){
	IJ.log(fnames[i]);
	run("Close All");
	open(fnames[i]);
	imgname = getTitle();
	run("Duplicate...", "title=binary.tif");
	run("Set Measurements...", "area mean center shape stack display redirect=None decimal=3");
	run("Set Scale...", "distance=0 known=0 unit=pixel global");
	run("Variance...", "radius=5");
	setAutoThreshold("Default dark");
	run("Convert to Mask");
	run("Analyze Particles...", "size=4000-90000 circularity=0.05-1.00 show=Masks display exclude clear summarize add");
	run("Fill Holes");
	roiManager("reset");
	run("Analyze Particles...", "summarize add");
	getResult("XM", 0);
	maxWidth = 350;
	selectWindow(imgname);
	nRois = roiManager("count");
	roiChoice = 0;
	for (iroi = 0; iroi < nRois; iroi++) {
	 	if (getResult("Round", iroi) > 0.8){
	 		if (getResult("AR", iroi) > 0.5) {
	 			roiChoice = iroi;
	 		}
	 	}
	}
	makeRectangle(getResult("XM", roiChoice) - maxWidth/2, getResult("YM", roiChoice) - maxWidth/2, maxWidth, maxWidth);
	run("Duplicate...", "title=cropped");
	saveAs("Tiff", File.getDirectory(fnames[i]) + File.separator + "cropped" + File.separator + File.getName(fnames[i]) );
}

