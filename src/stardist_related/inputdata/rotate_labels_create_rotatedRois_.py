@ File[] listOfFiles (label = "select labeled images", style = "file")
@ File (label = "tif directory", style = "directory")  dir_tifs
@ File (label = "nuclei mask directory", style = "directory") dir_mask_nuclei
@ File (label = "nuclei mask directory ws", style = "directory") dir_mask_nuclei_ws
@ File (label = "nucleoli mask directory", style = "directory") dir_mask_nucleoli





from ij import IJ
from ij.plugin.frame import RoiManager
from ij.measure  import ResultsTable
import os
import math
from ij.gui import PointRoi
import csv
from datetime import datetime

# Measure size of nucleoli mass with respect to nucleus and area total to area convex
# Use the results from manually corrected and aligned nucleoli. nuclei
# Counting is done with respect to center of mass

def get_rois_nucleoli(imp):
	"""
	Create rois from labeled image
	:param imp:
	"""
	rm = RoiManager.getRoiManager()
	rm.reset()
	rois_allframes = list()
	for iframe in range(0,imp.getNFrames()):
		imp.setT(iframe+1)
		stat = imp.getStatistics().max
		if stat == 0:
			continue
		for iobj in range(1, int(stat)+1):
			bin = imp.duplicate()
			bin.setT(iframe+1)
			IJ.setRawThreshold(bin, iobj, iobj, None)
			IJ.run(bin, "Convert to Mask",  "method=Default background=Dark black")
			IJ.run(bin, "Analyze Particles...", "add slice")
			bin.close()
	rois = rm.getRoisAsArray()
	return rois

def get_rois_nuclei(imp, ws):
	"""
	Create rois from labeled image. Creates 2 to 3 Rois depending if the nuclei are overlapping or not
	:param imp: the labelled image
	"""

	rm = RoiManager.getRoiManager()
	rm.reset()
	rois_allframes = list()
	for iframe in range(0, imp.getNFrames()):
		rm.reset()
		imp.deleteRoi()
		imp.setT(iframe+1)
		stat = imp.getStatistics().max
		print(stat)
		if ws: # order is inverted with respect to manual annotation for consistency keep the same numbering
			binImgs = [imp.duplicate(), imp.duplicate()]
			for idx, bin in enumerate(binImgs):
				IJ.setRawThreshold(bin, idx+1, idx+1, None)
				IJ.run(bin, "Convert to Mask",  "method=Default background=Dark black")
				bin.setT(iframe+1)
				IJ.run(bin, "Analyze Particles...", "add slice")
			rois = rm.getRoisAsArray()
			[rois_allframes.append(rois[idx]) for idx in [1,0]]
		else:
			if stat > 2:
				imp.deleteRoi()
				binImgs = [imp.duplicate(), imp.duplicate(), imp.duplicate()]
				IJ.setRawThreshold(binImgs[0], 1, 1, None)  # nucleus with label 1
				IJ.setRawThreshold(binImgs[1], 2, 3, None) 	# nucleus with label 2 and overlapping region
				IJ.setRawThreshold(binImgs[2], 3, 3, None)  # overlapping region

				for bin in binImgs:
					bin.deleteRoi()
					IJ.run(bin, "Convert to Mask",  "method=Default background=Dark black")
					bin.setT(iframe+1)
					IJ.run(bin, "Analyze Particles...", "add slice")

				# nucleus label 1 and overlapping region
				rm.setSelectedIndexes([0,2])
				rm.runCommand(bin, "Combine")
				roiCombine = bin.getRoi()
				roiCombine.setPosition(iframe+1)
				rm.addRoi(roiCombine)
				# reshuffle [label1+3, label2+3, label3]
				rois = rm.getRoisAsArray()
				[rois_allframes.append(rois[idx]) for idx in [3,1]]
			else: # no overlapping region
				imp.deleteRoi()
				binImgs = [imp.duplicate(), imp.duplicate()]
				for idx, bin in enumerate(binImgs):
					IJ.setRawThreshold(bin, idx+1, idx+1, None)
					IJ.run(bin, "Convert to Mask",  "method=Default background=Dark black")
					bin.setT(iframe+1)
					IJ.run(bin, "Analyze Particles...", "add slice")
				rois = rm.getRoisAsArray()
				[rois_allframes.append(rois[idx]) for idx in [0,1]]
	# center of mass
	rm.reset()
	[rm.addRoi(roi) for roi in rois_allframes]

	return rois_allframes

def compute_rotations(imp, rois):
	"""
		From nuclei Rois 1st and 2nd determine the center of mass
		Compute rotation and translation accordingly
		of two tracked object per slice
		Compute rotation angle (degrees) and translation vector
	"""
	width = imp.getWidth()
	height = imp.getHeight()

	delta_center = list()
	alpha = list()
	for iframe in range(0, imp.getNFrames()):
		imp.setT(iframe+1)
		CM = list()
		for roi in rois:
			print roi.getPosition()
			if roi.getPosition() == iframe+1:
				imp.deleteRoi()
				imp.setRoi(roi)
				CM.append([roi.getStatistics().xCenterOfMass, roi.getStatistics().yCenterOfMass])
		imp.deleteRoi()
		nuc_vec = [CM[1][0]-CM[0][0], CM[1][1]-CM[0][1]]
		nuc_vec_norm = math.sqrt(nuc_vec[0]*nuc_vec[0]+nuc_vec[1]*nuc_vec[1])
		nuc_vec = [nuc_vec[0]/nuc_vec_norm, nuc_vec[1]/nuc_vec_norm]
		alpha.append(90 + math.atan2(nuc_vec[0], nuc_vec[1])/math.pi*180)
		delta_center.append([width/2 - (CM[0][0] + CM[1][0])/2, height/2 - (CM[0][1] + CM[1][1])/2])

	return [delta_center, alpha]

def rotate_imp(imp, delta_center, alpha):
	for iframe in range(0, imp.getNFrames()):
		imp.setT(iframe+1)
		IJ.run(imp, "Translate...", "x="+str(delta_center[iframe][0])+" y="+str(delta_center[iframe][1])+" interpolation=None slice");
		IJ.run(imp, "Rotate... ", "angle="+str(alpha[iframe])+"  grid=1 interpolation=Bilinear slice");

def rotate_rois(imp, rois, delta_center, alpha):
	"""
		Rotate Rois
	"""
	rm = RoiManager()
	rm = rm.getInstance()
	rm.reset()
	[rm.addRoi(roi) for roi in rois]
	rm.runCommand(imp,"Show None")
	for iroi in range(0, rm.getCount()):

		rm.select(iroi)
		roi = rm.getRoi(iroi)
		slice = roi.getPosition()
		imp.setT(slice)
		imp.setRoi(roi)
		rm.translate(delta_center[slice-1][0], delta_center[slice-1][1])
		rm.deselect()
		rm.select(iroi)
		IJ.run(imp, "Rotate...", "rotate angle="+str(alpha[slice-1]))
		rm.runCommand(imp,"Update")

	rois = rm.getRoisAsArray()
	return rois

def get_rois_convex(rois, imp):
	"""
	compute convex shape of  set of ROIs
	:param imp: Labeled image
	:return: a combined roi and convex roi
	"""
	if len(rois) == 0:
		return ["", ""]

	rm = RoiManager()
	rm = rm.getInstance()
	rm.reset()
	[rm.addRoi(roi) for roi in rois]
	if rm.getCount() > 1:
		rm.setSelectedIndexes(range(0, rm.getCount()))
		rm.runCommand(imp,"Combine")
		roi_combined = imp.getRoi()
		rm.addRoi(roi_combined)
		IJ.run(imp, "Convex Hull", "")
		roi_convex = imp.getRoi()
		rm.addRoi(roi_convex)
		imp.setRoi(roi_combined)
		area_combined = imp.getStatistics().area
		imp.setRoi(roi_convex)
		area_convex = imp.getStatistics().area
		return [area_combined, area_convex]
	else:
		imp.setRoi(rois[0])
		area_combined = imp.getStatistics().area
		return [area_combined, area_combined]

def get_center_nuc(imp):
	"""
	 Obtain point of interface of the 2 nuclei, approximated as the middle between the centroids
	:param imp: labeled image with nuclei labels
	:return: outval = [X_interface, Y_interface, intersecetion_area/total_area, max_feret1+max_feret2 (this approximates the maximal length)]
	"""
	nrlabels = imp.getStatistics().max
	title = imp.getTitle()
	IJ.run(imp, "Analyze Regions", "area centroid max._feret")
	tablename = os.path.splitext(title)[0]  + "-Morphometry"
	rt = ResultsTable().getResultsTable(os.path.splitext(title)[0]  + "-Morphometry")
	if nrlabels == 2:
		# results table is Area, centroidX, centroidY, intersection area/total_area, length
		
		outval = [(rt.getColumn(1)[1] + rt.getColumn(1)[0])/2, 
		(rt.getColumn(2)[1] + rt.getColumn(2)[0])/2, 
		0, 
		rt.getColumn(3)[0] + rt.getColumn(3)[1]]
	else:
		# If 3 labels. The centroid of the 3rd label is the interface
		outval = [rt.getColumn(1)[2], 
		rt.getColumn(2)[2], 
		rt.getColumn(0)[2]/(rt.getColumn(0)[0]+rt.getColumn(0)[1]+rt.getColumn(0)[2]),
		rt.getColumn(3)[0]+rt.getColumn(3)[1]
		]
	
	IJ.selectWindow(tablename)
	IJ.run("Close")
	return  outval


def get_nucleoli_counts(nucleoli, nuclei):
	"""
	Count the nucleoli not used anymore
	:param nucleoli: nucleoli labaled image
	:param nuclei: labeled image
	:return: nr of nucleoli per nucleus and interface (if present). 
	"""
	IJ.run(nucleoli, "Analyze Regions", "area centroid max._feret")
	nrlabels = nucleoli.getStatistics().max
	title = nucleoli.getTitle()
	IJ.run(nucleoli, "Analyze Regions", "centroid")
	tablename = os.path.splitext(title)[0]  + "-Morphometry"
	rt = ResultsTable().getResultsTable(tablename)
	nucleoli_counts = [0,0,0]
	for ilab in range(0, int(nrlabels)):
		x = int(round(rt.getColumn(0)[ilab])) 
		y = int(round(rt.getColumn(1)[ilab]))
		lbl_value = int(nuclei.getPixel(x,y)[0])
		if lbl_value == 0:
			continue
		nucleoli_counts[lbl_value-1] = nucleoli_counts[lbl_value-1] + 1
	return nucleoli_counts		

outval = []
for img in listOfFiles:

	IJ.run("Close All")
	fname = img.getName()
	imp = IJ.openImage(os.path.join(dir_tifs.getPath(), fname))
	impNucleoli = IJ.openImage(os.path.join(dir_mask_nucleoli.getPath(), fname))
	impNuc = IJ.openImage(os.path.join(dir_mask_nuclei.getPath(), fname))
	impNucWs = IJ.openImage(os.path.join(dir_mask_nuclei_ws.getPath(), fname))
	roiNuc = get_rois_nuclei(impNuc, ws = 0)
	roiNucleoli = get_rois_nucleoli(impNucleoli)
	roiNucWs = get_rois_nuclei(impNucWs, ws = 1)
	[delta_center, alpha] = compute_rotations(impNuc, roiNucWs)


	rotate_imp(imp, alpha = alpha, delta_center = delta_center)
	imp.show()
	roiNucrotate = rotate_rois(impNuc,rois = roiNuc, alpha = alpha, delta_center = delta_center)
	roiNucWsrotate = rotate_rois(impNucWs,rois = roiNucWs, alpha = alpha, delta_center = delta_center)
	roiNucleoliRotate = rotate_rois(impNucleoli,rois = roiNucleoli, alpha = alpha, delta_center = delta_center)
	IJ.saveAs(imp, "Tiff", os.path.join(dir_tifs.getPath() + "_rotated", fname))
	rm = RoiManager()
	rm = rm.getInstance()

	rm.reset()
	[rm.addRoi(roi) for roi in roiNucWsrotate]
	rm.setSelectedIndexes(range(0, rm.getCount()))
	rm.runCommand("Save", os.path.join(dir_mask_nuclei_ws.getPath() + "_rotated", os.path.splitext(fname)[0] + ".zip"))


	rm.reset()
	[rm.addRoi(roi) for roi in roiNucleoliRotate]
	rm.setSelectedIndexes(range(0, rm.getCount()))
	rm.runCommand("Save", os.path.join(dir_mask_nucleoli.getPath() + "_rotated", os.path.splitext(fname)[0] + ".zip"))

	rm.reset()
	[rm.addRoi(roi) for roi in roiNucrotate]
	rm.setSelectedIndexes(range(0, rm.getCount()))
	rm.runCommand("Save", os.path.join(dir_mask_nuclei.getPath() + "_rotated", os.path.splitext(fname)[0] + ".zip"))



# imp.show()

	#IJ.saveAs("Tiff", os.path.join(dir_tifs.getPath() + "_rotated", fname))
	# impNucleoli.show()
	# impNuc.show()
	# nucleoli_counts = get_nucleoli_counts(nucleoli = impNucleoli, nuclei = impNuc)
	# nuc_interface = get_center_nuc(impNuc)
	# roi_nucleoli = get_rois_convex(impNucleoli)
	# roi_nuc = get_rois_convex(impNuc)
	#
	# rm = RoiManager.getRoiManager();
	# rm.reset()
	# IJ.run("Set Measurements...", "area centroid fit feret's redirect=None decimal=3");
	# # Add joint ROI of nuclei
	# rm.addRoi(roi_nuc[len(roi_nuc)-1])
	# for roi in roi_nucleoli:
	# 	rm.addRoi(roi)
	# rm.setSelectedIndexes(range(0, rm.getCount()))
	# rm.runCommand(impNucleoli, "Measure")
	#
	# rt = ResultsTable().getResultsTable()
	# # Get polygon of convexshape
	# # rm.addRoi(PointRoi(nuc_interface[0], nuc_interface[1]))
	#
	# poly = roi_nucleoli[1].getFloatPolygon()
	# spread = compute_spread_along_nuclei(nuc_interface, poly, rt.getValue("Angle", 0))
	# outval.append([fname, nuc_interface[2],  rt.getValue("Area", 1)/rt.getValue("Area", 2) , spread[0], spread[1], nuc_interface[3],
	# nucleoli_counts[0], nucleoli_counts[1], nucleoli_counts[2]])

# csv_file = open(os.path.join(dir_mask_nucleoli.getPath() ,datetime.today().strftime('%Y%m%d') + "_measure_geometry_fromLabels.csv"), 'wb')
# csv_writer = csv.writer(csv_file, delimiter = ',')
# header = ['file_name_t0_first_clear_frame_t1_last_clear_frame', 'ratio_nuc_area_overlap_nuc_area_total',
# 'ratio_nucleoli_area_nucleoli_area_convex', 'min_dot_nucleoli_nucDir','max_dot_nucleoli_nucDir', 'total_length_2_nuclei',
# 'nr_nucleoli_nucleus1', 'nr_nucleoli_nucleus2', 'nr_nucleoli_interface']
# csv_writer.writerow(header)
# for arow in outval:
# 	csv_writer.writerow(arow)
# csv_file.close()