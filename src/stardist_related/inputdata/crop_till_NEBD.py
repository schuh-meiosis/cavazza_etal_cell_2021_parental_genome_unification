import csv
import os
from ij import IJ
from ij.plugin import Duplicator
from ij.plugin import HyperStackConverter
maindir = u'Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/all_videos_20200803_cropped'
outdir = u'Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/all_videos_20200803_cropped_till_NEBD'
filereader = csv.DictReader(open(os.path.join(maindir, 'all_videos_v200930.csv')), 
delimiter = ",")
IJ.run("Close All")
IJ.resetEscape()
dp = Duplicator()
for row in filereader:
	name = os.path.splitext(row["file_name"])[0] + ".tif"
	fin_name = os.path.join(maindir, name)
	endpoint = row["frame_NEBD"]
	fout_name =  os.path.join(outdir, name)
	if IJ.escapePressed():
		break
	if os.path.exists(fin_name) & ~os.path.exists(fout_name):
		imp = IJ.openImage(fin_name)
		imp2 = dp.run(imp, 1, int(endpoint))
		imp3 = HyperStackConverter.toHyperStack(imp2, 1,1, int(endpoint))
		IJ.saveAs(imp3, "Tiff", fout_name)
		imp.close()
		imp2.close()
		imp3.close()
	