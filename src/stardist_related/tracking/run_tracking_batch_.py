#@ File (label = "imagefile directory", style = "directory") imgdir
#@ File (label="prediction directory", style="directory") roidir
#@ File (label="tracks directory", style="directory") trackdir
#@ ScriptService scriptService

# run tracking on image set and write out file track_labeled_image
import os
from ij import IJ, WindowManager
from ij.plugin.frame import RoiManager
from ij.gui import YesNoCancelDialog
from java.io import File
from ij.measure import ResultsTable
import os
import time
import glob 

def main():
    IJ.run("Close All")
    IJ.run("Clear Results", "")
    fnames = glob.glob(os.path.join(imgdir.getPath(), '*tif'))
    for imgname in fnames:
        fname =  os.path.splitext(os.path.basename(imgname))[0]
        zipinfile = os.path.join(roidir.getPath(), fname + ".zip")
        trackoutfile = os.path.join(trackdir.getPath(), fname + ".txt")
        if not os.path.exists(zipinfile):
            IJ.showMessage("Next image. Could not find file " + zipinfile)
            continue
        if os.path.exists(trackoutfile):
            continue
        else:
            break

    imp = IJ.openImage(imgname)
    imp.show()
    rm = RoiManager()
    rm = rm.getInstance()
    rm.reset()

    rm.runCommand("Open", zipinfile)

    IJ.run("Enhance Contrast", "saturated=0.35")
    IJ.run("Clear Results", "")
    IJ.run("Set Measurements...", "area center stack redirect=None decimal=3")
    rm.runCommand(imp, "Measure")
    scriptService.run(File("D:/Code/cavazzatommaso/src/scripts/stardist/track_labeled_image_.py"), True ,[]);
    rt = ResultsTable().getResultsTable()
    while not rt.columnExists("TrackN"):
    	time.sleep(5)
    IJ.saveAs("Results", trackoutfile);
    rm.runCommand("Show All")

main()
