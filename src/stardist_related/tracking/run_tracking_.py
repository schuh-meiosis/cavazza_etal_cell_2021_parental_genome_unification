#@ File (label = "imagefile") afile
#@ File (label="prediction directory", style="directory") roidir
#@ File (label="tracks directory", style="directory") trackdir
#@ String (label = "nucleus to process", choices= {"1", "2", "1+2", "together"}, style = "listbox") nuctoprocess
#@ ScriptService scriptService

# run tracking on image set and write out file track_labeled_image
import os
from ij import IJ, WindowManager
from ij.plugin.frame import RoiManager
from java.awt.event import KeyEvent, KeyAdapter
from ij.gui import YesNoCancelDialog
from java.io import File
from ij.measure import ResultsTable
import time

nuc_list = {"1":["_nuc1"], "2":["_nuc2"], "1+2":["_nuc1", "_nuc2"], 
"together":[""]}

def main():
    IJ.run("Close All")

    for nuc in nuc_list[nuctoprocess]:
	    fname = os.path.splitext(os.path.basename(afile.getPath()))[0]
	    zipinfile = os.path.join(roidir.getPath(), fname + nuc + ".zip")
	    trackoutfile = os.path.join(trackdir.getPath(), fname + nuc + ".txt")
	    if not os.path.exists(zipinfile):
	        IJ.showMessage("Exiting macro. Could not find file " + zipinfile)
	        return
	    if os.path.exists(trackoutfile):
	        msg = YesNoCancelDialog(IJ.getInstance(), "Manual qc Predictions", trackoutfile +"\nOutput file exists do you want to continue?")
	        if not msg.yesPressed():
	            return
	    fpath = os.path.dirname(afile.getPath())
	    imp = IJ.openImage(afile.getPath())
	    imp.show()
	    rm = RoiManager()
	    rm = rm.getInstance()
	    rm.reset()
	    
	    rm.runCommand("Open", zipinfile)
	    
	    IJ.run("Enhance Contrast", "saturated=0.35")
	    IJ.run("Clear Results", "")
	    IJ.run("Set Measurements...", "area center stack redirect=None decimal=3")
	    rm.runCommand(imp, "Measure")
	    scriptService.run(File("D:/Code/cavazzatommaso/src/scripts/stardist/track_labeled_image_.py"), True ,[]);
	    rt = ResultsTable().getResultsTable()
	    while not rt.columnExists("TrackN"):
	    	time.sleep(5)
	    IJ.saveAs("Results", trackoutfile);

main()
