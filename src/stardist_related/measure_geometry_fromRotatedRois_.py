@ File[]   (label = "rotated tif files", style = "file") tif_files
@ File (label = "rotated nucleoli roi directory", style = "directory") dir_roi_nucleoli
@ File (label = "rotated nuclei roi directory", style = "directory") dir_roi_nuclei
@ File (label = "output directory", style = "directory") dir_out
		
# dir mask nucleoli:
# dir_mask_nucleoli = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/qupath_2tpts/ground_truth/masks/"
# dir_mask_nuclei = "Z:/schuh_lab/Tommaso on Microscope Facility/human embryos/qupath_2tps_nuclei/ground_truth/masks/"


from ij import IJ
from ij.plugin.frame import RoiManager
from ij.measure  import ResultsTable
import os
import math
from ij.gui import PointRoi
import csv
from datetime import datetime
from ij import ImagePlus
from ij.process import ImageStatistics as ImageStatistics
from ij.measure import Measurements as Measurements
# Measure size of nucleoli mass with respect to nucleus and area total to area convex
# Use the results from Machine learning. Files are time-lapses now
# Counting is done with respect to center of mass

KEY_NUCLOLEI = "nucleoli"
KEY_NUCLEI = "nuclei"

def get_rois_convex(rois, imp):
	"""
	compute convex shape of  set of ROIs
	:param imp: Labeled image
	:return: a combined roi and convex roi
	"""
	if len(rois) == 0:
		return {'area_nucleoli': "", 'area_nucleoli_convex': "",
		 'xc_nucleoli_convex': "", 'yc_nucleoli_convex' : "" }

	rm = RoiManager()
	rm = rm.getInstance()
	rm.reset()
	[rm.addRoi(roi) for roi in rois]
	if rm.getCount() > 1:
		rm.setSelectedIndexes(range(0, rm.getCount()))
		rm.runCommand(imp,"Combine")
		roi_combined = imp.getRoi()
		rm.addRoi(roi_combined)
		IJ.run(imp, "Convex Hull", "")
		roi_convex = imp.getRoi()
		rm.addRoi(roi_convex)
		imp.setRoi(roi_combined)
		area_combined = imp.getStatistics().area
		imp.setRoi(roi_convex)
		area_convex = imp.getStatistics().area
		imp.setRoi(roi_convex)
		Cen = roi_convex.getContourCentroid()

		return {'area_nucleoli': area_combined, 'area_nucleoli_convex': area_convex,
				'xc_nucleoli_convex': Cen[0], 'yc_nucleoli_convex' : Cen[1] }
	else:
		imp.setRoi(rois[0])
		area_combined = imp.getStatistics().area
		imp.setRoi(rois[0])
		Cen = rois[0].getContourCentroid()
		return {'area_nucleoli': area_combined,
				'area_nucleoli_convex': area_combined,
				'xc_nucleoli_convex': Cen[0], 'yc_nucleoli_convex' :Cen[1]}

def create_rois_pernucleus(nucleoli, nuclei, imp):
	rm = RoiManager()
	rm = rm.getInstance()
	rm.reset()
	# containsPoint does not seem to work
	outval = list()

	# create a order left is always 0 and right always 1
	if nuclei[1].getContourCentroid()[0] < imp.getWidth()/2: # swap order
		nuclei = [nuclei[1], nuclei[0]]

def process_rois_perframe(nucleoli, nuclei, imp):
	"""
	Process rois in nucleoli and nuclei and compute distance to interface and opposite side for each nuclei.
	Compute only along x has nuclei have been registered
	Order of nuclei is arranged so that first nuclei is always on the left
	:param nucleoli: rois of nucleoli for current frame
	:param nuclei: rois of nuclei for current frame
	:param imp: image of zygote
	:return: a 2x8 list [inuc,  nuc_extrema[0], nuc_extrema[1],
						dist_to_border_roicentroid[0], dist_to_border_roicentroid[1],
					    dist_to_border_roipoly[0], dist_to_border_roipoly[1],
					    nuc_length]
			rois_nuclei order left to right
			rois_nucleoli per nucleus
	"""
	# find nucleoli per nucleus
	# containsPoint does not seem to work

	# create a order left is always 0 and right always 1
	if nuclei[1].getContourCentroid()[0] < imp.getWidth()/2: # swap order
		nuclei = [nuclei[1], nuclei[0]]
	nucleoli_nuc = [ None, None ]
	geom_frame = [None , None ]
	for inuc, nuc in enumerate(nuclei):
		nucleoli_nuc[inuc] = [roi for roi in nucleoli if nuc.contains(int(roi.getContourCentroid()[0]), int(roi.getContourCentroid()[1]))]
		geom_frame[inuc] = compute_distances(nuc, nucleoli_nuc[inuc], inuc)
		combined_convex_area  = get_rois_convex(nucleoli_nuc[inuc], imp)
		geom_frame[inuc].update(combined_convex_area)

	return [geom_frame, nuclei, nucleoli_nuc]

def compute_distances(nuc, nucleoli, inuc):
	"""
	Process nucleoli-rois in a nucleus and compute distance to interface and opposite side.
	Compute only along x has nuclei have been registered.
	Nucleus inuc = 0 is left nucleus.
	:param nuc: roi of current nucleus
	:param nucleoli: rois of nucleoli for current nucleus
	:param inuc:  index of nuclei
	:return: a 1x8 list [inuc,  nuc_extrema[0], nuc_extrema[1],
						dist_to_border_roicentroid[0], dist_to_border_roicentroid[1],
					    dist_to_border_roipoly[0], dist_to_border_roipoly[1],
					    nuc_length]

			nuc_extrema[0]([1]):  x-coordinate of outer (inner) membrane with respect to interface
	 		dist_to_border_centroid[0] ([1]): x-distance centroid to outer (inner) membrane
	 		dist_to_border_roipoly[0], ([1]): x-distance of nucleoli boundary to outer (inner)
	"""
	outval = []
	if len(nucleoli) == 0:
		outval = {'id_nuc': inuc,
				  'x_nuclei_outer': "",
				  'x_nuclei_inner': "",
				  'dist_to_outer_roicentroid': "",
				  'dist_to_inner_roicentroid': "",
				  'dist_to_outer_roipoly': "",
				  'dist_to_inner_roipoly':"",
				  'nuc_length':""}
		return outval

	poly = nuc.getFloatPolygon()
	max_poly = max(poly.xpoints)
	min_poly = min(poly.xpoints)
	if inuc == 0: # left side nuclei
		nuc_extrema = [min_poly, max_poly]
	else: # right side nuclei
		nuc_extrema = [max_poly, min_poly]
	nuc_length = max_poly-min_poly
	outer = [abs(roi.getContourCentroid()[0] - nuc_extrema[0]) for roi in nucleoli]
	idx_outer = outer.index(min(outer))
	roi_outer = nucleoli[idx_outer]
	poly_roi_outer = roi_outer.getFloatPolygon()
	dist_to_border_centroid = [abs(roi_outer.getContourCentroid()[0] - nuc_ex) for nuc_ex  in nuc_extrema]
	dist_to_border_poly = [min([abs(x - nuc_extrema[0]) for x in poly_roi_outer.xpoints]),
							   max([abs(x - nuc_extrema[1]) for x in poly_roi_outer.xpoints])]
	outval = {'id_nuc': inuc,
			  'x_nuclei_outer': nuc_extrema[0],
			  'x_nuclei_inner': nuc_extrema[1],
			  'dist_to_outer_roicentroid': dist_to_border_centroid[0],
			  'dist_to_inner_roicentroid': dist_to_border_centroid[1],
			  'dist_to_outer_roipoly': dist_to_border_poly[0],
			  'dist_to_inner_roipoly':dist_to_border_poly[1],
			  'nuc_length':nuc_length}
	return outval

def process_file(file_img, file_name_rois):
	"""
	Process a file and save rois for each nuclei in separated files.
	These will be used for the tracking.
	:param file_img: file name of imaging file
	:param file_name_rois: dictionary with file name of roi.zip files
	"""
	IJ.run("Close All")
	print(file_img)
	imgname = os.path.basename(file_img)
	rm_sorted = {KEY_NUCLOLEI: [RoiManager(0), RoiManager(0)],
				   KEY_NUCLEI: [RoiManager(0), RoiManager(0)]}
	for key in rm_sorted:
		[rml.reset() for rml in rm_sorted[key]]

	imp = IJ.openImage(file_img)
	imp.show()
	rm = RoiManager()
	rm = rm.getInstance()
	rois = {}
	for key in file_name_rois:
		rm.reset()
		rm.runCommand("Open", file_name_rois[key])
		rois[key] = rm.getRoisAsArray()
	
	rm.reset()
	# identify roi per slice
	geom_timelapse = list()

	frameMax = imp.getNFrames()+1

	for iframe in range(1, frameMax):
		nextframe = 0
		rois_slice = {}
		for key in rois:
			rois_slice[key] = [roi for roi in rois[key] if roi.getPosition() == iframe]
		for key in rois_slice:
			if len(rois_slice[key]) == 0:
				nextframe = 1
		if nextframe:
			continue
		[geom_frame, nuclei, nucleoli] = process_rois_perframe(rois_slice[KEY_NUCLOLEI], rois_slice[KEY_NUCLEI], imp)
		[g.update({'frame': iframe}) for g in geom_frame]
		[g.update({'imgname': imgname}) for g in geom_frame]
		[geom_timelapse.append(g) for g in geom_frame]
		[rm_sorted[KEY_NUCLEI][idx].addRoi(nuc) for idx, nuc in enumerate(nuclei)]

		
		if len(nucleoli[0]) > 0:
			[rm_sorted[KEY_NUCLOLEI][0].addRoi(roi) for roi in nucleoli[0]]
		if len(nucleoli[1]) > 0:
			[rm_sorted[KEY_NUCLOLEI][1].addRoi(roi) for roi in nucleoli[1]]
	# save the rois
	print(geom_timelapse)
	for key in rm_sorted:
		outname = os.path.splitext(file_name_rois[key])[0]
		[rmloc.runCommand("Save", outname + "_nuc" + str(idx+1) + ".zip") for idx, rmloc in enumerate(rm_sorted[key]) if rmloc.getCount() > 0]
	outname = os.path.splitext(file_name_rois[KEY_NUCLOLEI])[0]
	fname = os.path.basename(outname)
	outname = os.path.join(dir_out.getPath(), datetime.today().strftime('%Y%m%d') + '_' + fname + "_geometry_fromRotatedRois.csv")

	csv_columns = ['imgname', 'frame', 'id_nuc', 'x_nuclei_outer', 'x_nuclei_inner',
				   'area_nucleoli', 'area_nucleoli_convex', 'xc_nucleoli_convex', 'yc_nucleoli_convex',
				   'dist_to_outer_roicentroid', 'dist_to_inner_roicentroid',
				   'dist_to_outer_roipoly', 'dist_to_inner_roipoly',
				   'nuc_length']
	csv_file = open(outname, 'wb')

	csv_writer = csv.DictWriter(csv_file, delimiter = ',', fieldnames= csv_columns)
	csv_writer.writeheader()
	#csv_writer.writerow(['All units in pixels. Nuclei have been aligned to the horizontal axis. Outer is the border away from  the interface, '
	#					'inner is the interface. Distances is computer for the outermost nucleoli.'])
	#csv_writer.writerow(header)
	for data in geom_timelapse:
		csv_writer.writerow(data)
	# for arow in geom_timelapse:
	#  	csv_writer.writerow(arow)
	csv_file.close()

def main(tif_files, dir_roi_nucleoli, dir_roi_nuclei):
	"""

	:param tif_files: a list of java_io_file class
	:param dir_roi_nucleoli: java_io_file directory
	:param dir_roi_nuclei: java_io_file directory
	"""

	rm_nucleoli = RoiManager(0)
	rm_nuclei = RoiManager(0)
	rm_nuclei.reset()
	rm_nucleoli.reset()
	for img_file in tif_files:
		fname = os.path.splitext(img_file.getName())[0]
		file_name_rois = {KEY_NUCLOLEI: os.path.join(dir_roi_nucleoli.getPath(), fname + ".zip"),
						  KEY_NUCLEI: os.path.join(dir_roi_nuclei.getPath(), fname + ".zip")}
		for key in file_name_rois:
			if not os.path.exists(file_name_rois[key]):
				return
		process_file(img_file.getPath(), file_name_rois)



main(tif_files, dir_roi_nucleoli, dir_roi_nuclei)
