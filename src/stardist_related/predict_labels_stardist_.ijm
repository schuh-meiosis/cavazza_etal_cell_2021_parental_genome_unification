// Script to run a model on data
@ File[] myfiles
@ double( min = 0, max = 1) probThresh
@ double( min = 0, max = 1) nmsThresh
@ File model
@ String (choices= {"nucleoli", "nuclei"}, style = "listBox") prefixdir
@ File (style ="dir") outdir 
setBatchMode("hide");
model = replace(model, "\\", "/");

for (ifile = 0; ifile < myfiles.length; ifile++) {
	run("Close All");
	out_name = File.getNameWithoutExtension(myfiles[ifile]);
	out_name = outdir + "/" + prefixdir + "_"+probThresh +"_" +nmsThresh +"/" + out_name + ".zip";
	work_dir = File.getDirectory(out_name);
	if (!File.exists(work_dir)){
		File.makeDirectory(work_dir);
	}
	
	if (File.exists(out_name)){
		print(out_name + " exists!");
	}else{
		print("Processing  " + myfiles[ifile]);
		open(myfiles[ifile]);
		name = getTitle();
		run("Command From Macro", "command=[de.csbdresden.stardist.StarDist2D], args=['input':'"+name+
		"', 'modelChoice':'Model (.zip) from File', 'normalizeInput':'true', 'percentileBottom':'1.0', 'percentileTop':'99.8', 'probThresh':'"+probThresh+
		"', 'nmsThresh':'"+nmsThresh+
		"', 'outputType':'Both', 'modelFile':'"+model+
		"', 'nTiles':'1', 'excludeBoundary':'2', 'roiPosition':'Automatic', 'verbose':'false', 'showCsbdeepProgress':'false', 'showProbAndDist':'false'], process=[false]");
		roiArray = newArray(roiManager("count"));
		for (i = 0; i < roiArray.length; i++){
			roiArray[i] = i;
		}
		File.exists(out_name);
		roiManager("Select", roiArray);
		
		roiManager("Save", out_name);


	}
}