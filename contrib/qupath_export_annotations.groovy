/* QuPath-Script to export annotations to label tif images (e.g. to be used for stardist) 

 Use "Run for project" to export annotations for all images within a QuPath project

 Afterwards both images and mask tiffs will be stored in the project subfolder 
 
 ground_truth
 ├── images
 └── masks
 
 Based on code by Olivier Burri, Romain Guiet (both https://biop.epfl.ch/) and https://forum.image.sc/t/export-qupath-annotations-for-stardist-training/37391/3
 Modifed to account for multi timepoints
 
*/


// USER SETTINGS
def channel_of_interest = 1 // null to export all the channels 
def downsample = 1
def overlapnotation = 1 // True if additional ROis are genareted for overlapping labels

def image_name = getProjectEntry().getImageName()


def rm = RoiManager.getRoiManager() ?: new RoiManager()
// create an annotation of the entire image
createSelectAllObject(true)
def fullimage_annotation = getSelectedObject()


imageData = getCurrentImageData();
server = imageData.getServer();
viewer = getCurrentViewer();
hierarchy = getCurrentHierarchy();
//request = RegionRequest.createInstance(imageData.getServerPath(), downsample, fullimage_annotation.getROI())
for (iT = 0; iT < server.nTimepoints(); iT++){
    request = RegionRequest.createInstance(imageData.getServerPath(), downsample, 0, 0 , server.getWidth(), server.getHeight(), 0, iT);
    
    pathImage = null;
    pathImage = IJExtension.extractROIWithOverlay(server, fullimage_annotation, hierarchy, request, false, viewer.getOverlayOptions());
    image = pathImage.getImage();
    // Create the Labels image
    def labels = IJ.createImage( "Labels", "16-bit black", image.getWidth(), image.getHeight() ,1 );    
    rm.reset()
    IJ.run(image, "To ROI Manager", "")
    
    def rois = rm.getRoisAsArray() as List
    if (rois.size() == 0){
      println(image_name + " Image has not been fully annotated.");
      continue;
    }
    def idxSelArray = [];
    
    rois.eachWithIndex{ roi, idxSel ->
       
       if (roi.getType() == Roi.RECTANGLE) {
           println("Ignoring Rectangle")
       } else {
             idxSelArray << idxSel;      
        }
    }
    if (overlapnotation){
        rm.setSelectedIndexes(idxSelArray as int[])
        rm.runCommand(image, "AND");
        if(image.getRoi()){
            rm.runCommand(image, "Add");}
        rm.deselect();
    }
    rois = rm.getRoisAsArray() as List
    def label_ip = labels.getProcessor()
    def idx = 0
    rois.each{ roi ->
       if (roi.getType() == Roi.RECTANGLE) {
           println("Ignoring Rectangle")
       } else {
        println("T position :"+ iT +  " " +  roi.getPosition())
        label_ip.setColor( ++idx )
        label_ip.setRoi( roi )
        label_ip.fill( roi )
        }
    }
   
    labels.setProcessor( label_ip )
    
     // Split to keep only channel of interest
    def output = image
    if  ( channel_of_interest != null){
        imp_chs =  ChannelSplitter.split( image )
        output = imp_chs[  channel_of_interest - 1 ]
    }

    saveImages(output, labels, image_name, iT)
                
    println( image_name + " Image and Mask Saved." )
        
    // Save some RAM
    output.close()
    labels.close()
    image.close()

    // remove the fullimage_annotation from the list of annotations
    // (true is to keep descendant objects)
    
}
removeObject(  fullimage_annotation, true )
    // Remove extension from file name
    // Ignore file names starting with .
def removeExtension(def fileName) {
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        }
 
}
// This will save the images in the selected folder
def saveImages(def images, def labels, def name, def iT) {
    def source_folder = new File ( buildFilePath( PROJECT_BASE_DIR, 'ground_truth', 'images' ) )
    def target_folder = new File ( buildFilePath( PROJECT_BASE_DIR, 'ground_truth', 'masks' ) )
    mkdirs( source_folder.getAbsolutePath() )
    mkdirs( target_folder.getAbsolutePath() )
    def fname_img = removeExtension( new File (source_folder, name ).getAbsolutePath())
    def fname_lab = removeExtension( new File (target_folder, name ).getAbsolutePath())
    IJ.save( images , fname_img + "_t" + iT )
    IJ.save( labels , fname_lab + "_t" + iT  )
}


// Manage Imports
import qupath.lib.roi.RectangleROI
import qupath.imagej.gui.IJExtension;
import ij.IJ
import ij.gui.Roi
import ij.plugin.ChannelSplitter
import ij.plugin.frame.RoiManager
print "done"