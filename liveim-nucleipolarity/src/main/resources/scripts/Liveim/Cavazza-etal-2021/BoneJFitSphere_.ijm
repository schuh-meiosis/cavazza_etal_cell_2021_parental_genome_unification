// Call boneJ Fit Sphere to fit the point to a sphere and save the zip file.

// Author: Antonio Politi, MPIBPC, Goettingen
// Script associated to Cavazza et al. 2021
// make sure  that save_row is disabled

run("Input/Output...", "jpeg=85 gif=-1 file=.csv use_file copy_row save_column");

roizipfile = File.directory + "/" + File.nameWithoutExtension + "_01.zip";
if(File.exists(roizipfile)){
	roizipfile = File.directory + "/" + File.nameWithoutExtension + "_02.zip";
}
roiManager("Save", roizipfile);
run("Fit Sphere", "  padding=2 crop=1 clear_roi_manager");
roiManager("Delete");
roiManager("Show All");