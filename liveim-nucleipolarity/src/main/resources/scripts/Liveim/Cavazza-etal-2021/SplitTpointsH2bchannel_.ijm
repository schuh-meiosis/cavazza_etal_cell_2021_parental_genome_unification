#@ Integer (labels = "H2B Channel", value = 2) h2bchannel

// load a czi file and save all time points from the channel h2bchannel in separate files

// Author: Antonio Politi, MPIBPC, Goettingen
// Script associated to Cavazza et al. 2021

open()
setBatchMode(true)
fname = File.nameWithoutExtension;
print(fname)
path = File.directory;
print(path)
id = getImageID();
getDimensions(width, height, channels, slices, frames);
foldername = "input_images_C0" + h2bchannel
File.makeDirectory(path + foldername);
for (i = 1; i <= frames; i++){
	selectImage(id);
	run("Duplicate...", "duplicate frames=" + i + " channels=" + h2bchannel);
	//run("RGB Color", "slices");
	selectImage(fname + "-1.czi");
	
	saveAs("Tiff", path + foldername + "/" + fname  + "_C0" + h2bchannel + "_T" + IJ.pad(i,2) + ".tif");
	close();
}
run("Close All");