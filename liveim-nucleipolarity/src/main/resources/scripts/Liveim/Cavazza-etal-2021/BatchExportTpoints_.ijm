#@ File[]  (label = "Select czi files", style = "files")  listOfPaths

// Export single time points for later apply BoneJ fit sphere
// Author: Antonio Politi, MPIBPC, Goettingen
// Script associated to Cavazza et al. 2021
setBatchMode(true)
for (k=0;k<listOfPaths.length;k++) {
    myFile=listOfPaths[k];
	
	open(myFile);


	fname = File.nameWithoutExtension;
	print(fname);
	path = File.directory;
	path=path + File.separator + fname;
	File.makeDirectory(path);
	File.makeDirectory(path + File.separator + "input_images_C02" );
	id = getImageID();
	getDimensions(width, height, channels, slices, frames);
	
	
	
	for (i = 0; i <= 0; i++){
		selectImage(id);
		run("Duplicate...", "duplicate frames=" + i + " channels=2");
		//run("RGB Color", "slices");
		selectImage(fname + "-1.czi");
		saveAs("Tiff", path + File.separator +  "input_images_C02/" + fname  + "_C02_T" + IJ.pad(i,2) + ".tif");
		close();
	}
	run("Close All");
}