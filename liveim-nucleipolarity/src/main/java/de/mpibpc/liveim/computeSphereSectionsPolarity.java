package de.mpibpc.liveim;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.Commands;
import inra.ijpb.plugins.ExtendedMinAndMax3DPlugin;
import net.imagej.Dataset;
import net.imagej.ImageJ;
import net.imagej.ImgPlus;
import net.imagej.axis.Axes;
import net.imagej.axis.AxisType;
import net.imglib2.*;
import net.imglib2.RandomAccess;
import net.imglib2.algorithm.labeling.ConnectedComponents;
import net.imglib2.algorithm.region.hypersphere.HyperSphere;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgs;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.roi.Regions;
import net.imglib2.roi.labeling.ImgLabeling;
import net.imglib2.roi.labeling.LabelRegion;
import net.imglib2.roi.labeling.LabelRegions;
import net.imglib2.type.Type;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.util.LinAlgHelpers;
import net.imglib2.view.Views;
import org.apache.commons.io.FilenameUtils;
import org.scijava.ItemIO;
import org.scijava.ItemVisibility;
import org.scijava.command.Command;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;
import sc.fiji.simplifiedio.SimplifiedIO;
import java.io.File;
import java.util.*;
import java.util.Iterator;
import java.util.regex.Pattern;
import static java.lang.Math.round;


@Plugin(type= Command.class, menuPath = "Liveim > Cavazza-etal-2021 > nuclei polarity ")
public class computeSphereSectionsPolarity implements Command {
    @Parameter
    private ImageJ ij;

//    @Parameter(visibility = ItemVisibility.MESSAGE, required=false)
//    private final String header = "File with spheres coordinates";

    @Parameter(type = ItemIO.INPUT, label = "csv-file", style = "file", visibility = ItemVisibility.NORMAL)
    private File csvfile;

    @Parameter(type = ItemIO.INPUT, label = "out-directory (empty = current)", required = false, style = "directory", visibility = ItemVisibility.NORMAL)
    private File outdir;

    @Parameter(type = ItemIO.INPUT, label = "nr. sections", visibility = ItemVisibility.NORMAL)
    private int nrsections = 3;

    @Parameter(type = ItemIO.INPUT, label = "Compute)", choices = {"Polarity", "Distance-transform", "Polarity+Distance-transform"},
            visibility = ItemVisibility.NORMAL)
    private String tocompute;

    @Parameter(type = ItemIO.INPUT, label = "extendedMaxima(slow)", visibility = ItemVisibility.NORMAL)
    private boolean extendedMaxima = true;

    @Parameter(type = ItemIO.INPUT, label = "Maximal number of gauss-steps (slow)", visibility = ItemVisibility.NORMAL)
    private boolean allsigmas = true;

    @Parameter(type = ItemIO.INPUT, label = "show Output", visibility = ItemVisibility.NORMAL)
    private boolean showOutput = true;

    @Parameter(type = ItemIO.INPUT, label = "Overwrite", visibility = ItemVisibility.NORMAL)
    private boolean overwrite = true;


    private double[][] sigmas_g_short =  {{0, 0, 0}, {8.0, 8.0, 0}};
    private double[][] sigmas_g_long = {{0, 0, 0}, {8.0, 8.0, 0}, {16.0, 16.0, 0.0}, {16.0, 16.0, 1.0},
            {32.0, 32.0, 0.0}, {32.0, 32.0, 1.0}, {32.0, 32.0, 2.0}};

    @Override
    public void run() {
        try {
            ExtendedMinAndMax3DPlugin.Operation em = ExtendedMinAndMax3DPlugin.Operation.fromLabel("Extended Maxima");;
        } catch(NoClassDefFoundError e){
            IJ.log("Please install MorphoLibJ! Enable the corresponding update site, [ Help > Update > Manage update sites > [x] IJPB-plugins ] ! ");
            return;
        }

        String maindir = csvfile.getParent();
        if (maindir == null)
            return;
        if (outdir == null || !outdir.isDirectory()) {
            outdir = new File(maindir);
        }
        if (!outdir.isDirectory()) {
            IJ.log("Output directory does not exists");
            return;
        }

        String[] fileparts = maindir.split(Pattern.quote(File.separator));
        String expername = fileparts[fileparts.length - 2];
        List<String[]> spheres = new ArrayList<>();

        String[] header_file_directions = {"Filename", "XY_um_px","Z_um_px",  "X_sphere", "Y_sphere", "Z_sphere", "R_sphere",
                "X_dirnuc", "Y_dirnuc", "Z_dirnuc", "X_dirmaxpol", "Y_dirmaxpol", "Z_dirmaxpol"};
        File directions_file = new File(FilenameUtils.concat(outdir.getPath(), expername + "_directions.csv"));


        IJ.log(utils.readCsv(csvfile.getPath(), spheres));

        // Parse file entries with coordinate of the spheres must be at least 2!!
        for (int ifile = 1; ifile < spheres.size(); ifile += 2) {
            if (!spheres.get(ifile)[0].equals(spheres.get(ifile + 1)[0])) {
                IJ.log("could not process " + spheres.get(ifile)[0] + "as no 2 spheres were marked for current time points");
                continue;
            }
            String imgpath = maindir + File.separatorChar + spheres.get(ifile)[0];
            final File fileImg = new File(imgpath);
            final File fileDT = new File(FilenameUtils.concat(outdir.getPath(), FilenameUtils.getBaseName(imgpath) + "_DT.tif"));
            final File filePol = new File(FilenameUtils.concat(outdir.getPath(), FilenameUtils.getBaseName(imgpath) + "_polarity.tif"));
            if (!fileImg.exists() || fileImg.isDirectory()) {
                IJ.log("could not find the file " + spheres.get(ifile)[0]);
                continue;
            }
            IJ.log("Processing " + spheres.get(ifile)[0]);
            boolean DT = false;
            boolean pol = false;
            if (tocompute.contains("Distance-transform"))
                DT = true;
            if (tocompute.contains("Polarity")) {
                pol = true;
                if (overwrite)
                    directions_file.delete();
            }
            if (!overwrite) {
                if (DT && fileDT.exists()) {
                    DT = false;
                    IJ.log("Distance-transform has already been computed!");
                }

                if (pol && filePol.exists() && directions_file.exists()) {
                    pol = false;
                    IJ.log("Polarity has already been computed!");
                }
                if (!pol && !DT)
                    continue;
            }
            Object obj = null;

            try {
                obj = ij.io().open(fileImg.getPath());
            } catch (Exception e) {
                e.printStackTrace();
                IJ.log("could not open the file " + spheres.get(ifile)[0]);
                continue;
            }

            ImgPlus img =  ij.convert().convert(obj, ImgPlus.class);
            int[] dim_idx = {img.dimensionIndex(Axes.X), img.dimensionIndex(Axes.Y), img.dimensionIndex(Axes.Z)};
            double[] cal = {img.averageScale(dim_idx[0]),
                    img.averageScale(dim_idx[1]),
                    img.averageScale(dim_idx[2])};
            double[][] sph = utils.getSphereParameters(spheres.get(ifile), spheres.get(ifile+1), cal);
            Commands.closeAll(); //            IJ.run("Close All"); this cause a failed recording
            final double[][] dirNuclei = new double[2][3];
            final double[][] dirMaxPol = new double[2][3];
            ImgPlus<UnsignedByteType> out_imgDT;

            if (DT) {

                out_imgDT = this.processImageDT(img, sph, dirNuclei, dirMaxPol);
                SimplifiedIO.saveImage(out_imgDT, fileDT.getPath());
                if (showOutput) {
                    ij.ui().show(out_imgDT);
                }
            }

            if (!pol)
                continue;

            final ImgPlus<UnsignedByteType> out_imgPol = this.processImage(img, sph, dirNuclei, dirMaxPol);

            // an imagePlus with
            // outimg_plus = ij.convert().convert(ij.dataset().create(out_img), ImagePlus.class); for 3 channels this becomes RGB!!

            final List<String[]> dataout = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                dataout.add(new String[]{spheres.get(ifile + i)[0], "" + cal[0],"" + cal[2],
                        "" + sph[i][0], "" + sph[i][1], "" + sph[i][2],"" + sph[i][3],
                        "" + dirNuclei[i][0], "" + dirNuclei[i][1], "" + dirNuclei[i][2],
                        "" + dirMaxPol[i][0], "" + dirMaxPol[i][1], "" + dirMaxPol[i][2]});
            }
            utils.writeDataAtOnce(directions_file, header_file_directions, dataout, true);
            SimplifiedIO.saveImage(out_imgPol, filePol.getPath());

            if (showOutput) {
                Dataset d3 = ij.dataset().create(out_imgPol);
                d3.setRGBMerged(false);
                ij.ui().show(d3);
            }
        }
    }



    /**
     * @param val a vector
     * @return if at least element in vector is negative
     */
    private Boolean hasNegativeElement(double[] val){
        for (double v:val){
            if (v < 0)
                return true;
        }
        return false;
    }

    /**
     * Create 2 3D-spheres
     * Pixel value at the overlap are assigned to one sphere only according to the intersection plane
     * @param img an empty image in which one draw isotropic spheres has only one channel
     * @param sphi Sphere parameters {{center_x1, center_y1, center_z1, radius1}, {center_x2, center_y2, center_z2, radius2})
     */
    private void createSpheres(Img<UnsignedByteType> img, long[][] sphi) {

        if (sphi.length != 2)
            throw new IllegalArgumentException("nr of spheres must be 2 ");
        if (sphi[0].length != 4)
            throw new IllegalArgumentException("nr of sphere parameters slices must be 4 : center_x, center_y, center_z, radius ");

        // Create spheres
        List<HyperSphere<UnsignedByteType>> hyperSpheres = new ArrayList<>();
        // help variable for center of sphere
        for (int i = 0; i < sphi.length; i++) {
            HyperSphere<UnsignedByteType> asphere =
                    new HyperSphere<>(Views.extendZero(img), new Point(sphi[i][0], sphi[i][1], sphi[i][2]), sphi[i][3]);
            hyperSpheres.add(asphere);
        }

        // assign to each pixel belonging to sphere a value of 1, 2, or 3 if overlapping
        for (int i = 0; i < sphi.length; i++) {
            for (final UnsignedByteType value : hyperSpheres.get(i))
                value.setInteger(i + 1 + value.getInteger());
        }
        // TODO: This is not generic. If the labels are not sequential it fails

        // reassign overlapping pixels
        ArrayList<Integer> labels = new ArrayList<>(Arrays.asList(1, 2, 3));
        final ImgLabeling<Integer, UnsignedByteType> imgLabel = ImgLabeling.fromImageAndLabels(img, labels);
        LabelRegions<Integer> labelRegionsSpheres = new LabelRegions<>(imgLabel);
        final double[] norm = utils.planeNormal(new double[] {sphi[0][0], sphi[0][1], sphi[0][2]},
                new double[] {sphi[1][0], sphi[1][1], sphi[1][2]}, sphi[0][3], sphi[1][3]);
        if (labelRegionsSpheres.getExistingLabels().contains(3)) { // Spheres are overlapping
            final LabelRegion<Integer> region = labelRegionsSpheres.getLabelRegion(3);
            final Cursor<Void> cursor = region.cursor();
            final RandomAccess<UnsignedByteType> access = img.randomAccess();
            // Normal plane to both spheres
            while (cursor.hasNext()) {
                cursor.fwd();
                final double dist = utils.distanceToPlane(cursor, norm);
                access.setPosition(cursor);
                if (dist <= 0)
                    access.get().set(1);
                else
                    access.get().set(2);
            }
        }
    }

    /**
     * Clear all pixels expect the pixels in region
     * @param input input
     * @param region a labelregion
     * @param <T> generic type
     * @return an image with cleared pixels
     */
    private < T extends Type< T >> RandomAccessibleInterval<T> clearOutsideRegion(Img<T> input,  LabelRegion<Integer> region){
        final RandomAccessibleInterval<T> output = input.factory().create(input);
        final RandomAccess<T> outputRA = output.randomAccess();
        final RandomAccess<T> inputRA = input.randomAccess();
        final Cursor<Void> cursor = region.cursor();
        while (cursor.hasNext()) {
            cursor.fwd();
            outputRA.setPosition(cursor);
            inputRA.setPosition(cursor);
            //System.out.println(input.get());
            outputRA.get().set(inputRA.get());
        }
        return output;
    }


    /**
     * Create labels for sphere sections in an image according to regions and sphere_center coordinates.
     *
     * @param img_sphere   img where to create label
     * @param region_sphere region in image corresponding to the whole sphere (nucleus)
     * @param sphere XYZR coordinate of nuclei (isotropic)
     * @param labels labels to assign to sections
     * @param direction direction vector in which to orient the sections (isotropic)
     * @param sample a vector describing the scaling
     */
    private  void createSphereSections(RandomAccessibleInterval<UnsignedByteType> img_sphere, LabelRegion<Integer> region_sphere, long[] sphere,
                                       Integer[] labels, double[] direction, double[] sample) {

        //Sanity checks
        if (nrsections > 9)
            throw new IllegalArgumentException("nr slices must be lower than 9 ");
        if (sphere.length != 4)
            throw new IllegalArgumentException("nr of sphere parameters slices must be 4 : center_x, center_y, center_z, radius ");
        if (labels.length != nrsections)
            throw new IllegalArgumentException("nr of labels must match the number of sections");
        // Assign index values according to direction (this should be general)
        RandomAccess<UnsignedByteType> access = img_sphere.randomAccess();

        final double[] pos = new double[3];
        final Cursor<Void> cursor = region_sphere.cursor();
        while (cursor.hasNext()){
            cursor.fwd();
            cursor.localize(pos);
            // correct sampling
            pos[0] = pos[0]*sample[0];
            pos[1] = pos[1]*sample[1];
            pos[2] = pos[2]*sample[2];
            // check direction
            LinAlgHelpers.subtract(pos, new double[] {sphere[0], sphere[1], sphere[2]},  pos);
            double h = LinAlgHelpers.dot(pos, direction)/sphere[3];
            for (int j = 0; j < nrsections + 1; j++){
                if (h < 1 - 2.0/nrsections*j & h >= 1 - 2.0/nrsections*(j+1)){
                    try {
                        access.setPosition(cursor);
                        access.get().set(labels[j]);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        // No message
                    }
                }

            }
        }
    }

    /**
     * compute mean of intensity in image in sphere-sections along orientation
     * @param img image from which to compute the mean
     * @param region region indicating the sphere (nuclei)
     * @param sphere Coordinate of sphere
     * @param orientation vector for orientation
     * @param sample    sampling in XYZ sphere
     * @return a normalized vector in direction of polarity
     */
    private double[] computePolarity(RandomAccessibleInterval<UnsignedByteType> img,
                                     LabelRegion<Integer> region, long[] sphere,
                                     double[] orientation, double[] sample){

        try {
            RandomAccessibleInterval<UnsignedByteType> img_sections = ArrayImgs.unsignedBytes(img.dimension(0),
                    img.dimension(1), img.dimension(2));

            Integer[] sections = new Integer[nrsections];
            Arrays.setAll(sections, i -> i + 1);
            int idx = 0;
            double[] polarity = new double[nrsections];

            // compute a vector of direction and sections for eacg of the nuclei
            createSphereSections(img_sections, region, sphere, sections, orientation, sample);
            final ImgLabeling<Integer, UnsignedByteType> imgSectionLabels =
                    ImgLabeling.fromImageAndLabels(img_sections, new ArrayList<>(Arrays.asList(sections)));
            final LabelRegions<Integer> labelRegionsSections = new LabelRegions<>(imgSectionLabels);
            for (Integer i : sections) {
                polarity[i - 1] =
                        ij.op().stats().mean(Regions.sample(labelRegionsSections.getLabelRegion(i), img)).getRealDouble();

            }
            // normalize
            double sumA = Arrays.stream(polarity).sum();
            for (int i = 0; i < nrsections; i++)
                polarity[i] = polarity[i] / sumA;
            return polarity;
        } catch(NullPointerException e){
            IJ.log("Missing sphere sections in image. Incomplete sphere?");
            return new double[] {0.0, 0.0, 0.0};
        }

    }

    /**
     * Run polarity computation for several orientations and identify the maximal
     * Computes mean in sections along orientation(s). Find the largest normalized mean intensity
     * in first section.
     * @param img  img from which to compute mean
     * @param region the nucleus
     * @param sphere XYZR
     * @param orientations a combinations of vectors (isotropic)
     * @param scaling XYZ rescaling
     * @return index of orientation where maximal polarity is achieved
     */
    private int computeMaxPolarity(RandomAccessibleInterval<UnsignedByteType> img,
                               LabelRegion<Integer> region, long[] sphere,
                               List<double[]> orientations, double[] scaling){

        RandomAccessibleInterval<UnsignedByteType> img_sections = ArrayImgs.unsignedBytes(img.dimension(0),
                img.dimension(1), img.dimension(2));

        Integer[] sections = new Integer[nrsections];
        Arrays.setAll(sections, i -> i + 1);
        int idx = 0;
        double[][] polarity = new double[orientations.size()][nrsections];
        for (double[] orientation : orientations)  {
            polarity[idx] = computePolarity(img, region, sphere, orientation, scaling);
            idx++;
        }
        // find maximal value
        idx = 0;
        double maxValue  = 0;
        int idxMax = 0;
        for (double[] pol : polarity){
            if (pol[0] > maxValue){
                idxMax = idx;
                maxValue = pol[0];
            }
            idx++;
        }

       return idxMax;

    }

    /**
     * Compute for several images that have been gaussian filtered according to sigmas the center of gravity
     * and the extended maxima along with center of Mass.
     * @param img   An image
     * @param sigmas   3 values entries for size of gaussian filter in X, Y, and Z
     * @param sphere   Coordinate of sphere X_center, Y_center, Z_center, Radius
     * @param sample   Sampling long X, Y and Z coordinates
     * @return a list of vectors for the orientations computed for each gaussian blur
     */
    private List<double[]> computeOrientations(RandomAccessibleInterval<UnsignedByteType> img,
                                               double[][] sigmas, double[] sphere, double[] sample){
        List<RandomAccessibleInterval<UnsignedByteType>> gaussFilterImg = new ArrayList<>();
        for (double[] sigma : sigmas) {
            gaussFilterImg.add(ij.op().filter().gauss(img, sigma));
        }
        List<double[]> orientations = new ArrayList<>();
        for (RandomAccessibleInterval<UnsignedByteType> img_g : gaussFilterImg){
            final double[] posdiff = new double[3];
            final RealLocalizable cog = ij.op().geom().centerOfGravity((IterableInterval<UnsignedByteType>) img_g);
            cog.localize(posdiff);
            for (int i = 0; i < posdiff.length; i++)
                posdiff[i] = posdiff[i]*sample[i];
            LinAlgHelpers.subtract(posdiff, sphere, posdiff);
            LinAlgHelpers.normalize(posdiff);
            orientations.add(posdiff);
        }

        if(!extendedMaxima) {
            return orientations;
        }
        final long[] dims = new long[ img.numDimensions() ];
        img.dimensions(dims);
        final ConnectedComponents.StructuringElement se = ConnectedComponents.StructuringElement.FOUR_CONNECTED;
        final Iterator< String > labelCreator = new Iterator< String >()
        {
            int id = 0;

            @Override
            public boolean hasNext()
            {
                return true;
            }

            @Override
            public synchronized String next()
            {
                return "l" + ( id++ );
            }
        };
        // compute for each image extended maxima using MorphoLibJ.
        // This gives a set of possible coordinates where to compute the maximal polarity
        List<RealLocalizable>  directions = new ArrayList<>();
        for (RandomAccessibleInterval<UnsignedByteType> img_g : gaussFilterImg.subList(1, sigmas.length)) {
            final Img<IntType> labelImg = ArrayImgs.ints( dims );

            final ImgLabeling< String, IntType > labeling = new ImgLabeling<>( labelImg );
            // this is a hack to use the MorphoLibJ library that only works with images in memory
            ImagePlus imp = ImageJFunctions.wrap(img_g, "Gauss").duplicate();
            ImageStack stack = imp.getStack();
            ExtendedMinAndMax3DPlugin.Operation em = ExtendedMinAndMax3DPlugin.Operation.fromLabel("Extended Maxima");
            ImageStack result = em.apply(stack, 3, 6);
            ImagePlus impr = new ImagePlus("gauss", result);
            final Img <BitType> mask = ImageJFunctions.wrap(impr);
            ConnectedComponents.labelAllConnectedComponents(mask, labeling, labelCreator, se );
            final LabelRegions< String > labelRegions = new LabelRegions<>( labeling );
            for (final LabelRegion< String > region : labelRegions){
                final double[] posdiff = new double[3];
                RealLocalizable cog = region.getCenterOfMass();
                cog.localize(posdiff);
                for (int i = 0; i < posdiff.length; i++)
                    posdiff[i] = posdiff[i]*sample[i];
                LinAlgHelpers.subtract(posdiff, sphere, posdiff);
                LinAlgHelpers.normalize(posdiff);
                orientations.add(posdiff);
            }
        }
        return orientations;

    }


    private ImgPlus<UnsignedByteType> processImageDT (ImgPlus<UnsignedByteType> img, double[][] sph, double[][] dirNuclei, double[][] dirMaxPol) {

        int[] dim_idx = {img.dimensionIndex(Axes.X), img.dimensionIndex(Axes.Y), img.dimensionIndex(Axes.Z)};
        long[] dim = new long[img.numDimensions()];
        img.dimensions(dim);
        double[] cal = {img.averageScale(dim_idx[0]),
                img.averageScale(dim_idx[1]),
                img.averageScale(dim_idx[2])};
        // Contains DNA image, binary of spheres, distance transform of spheres

        ImgPlus<UnsignedByteType> out_img = new ImgPlus(ArrayImgs.unsignedBytes(dim[0], dim[1], dim[2], 2),
                FilenameUtils.getBaseName(img.getName()) + "_polarity.tif",
                new AxisType[]{Axes.X, Axes.Y, Axes.Z, Axes.CHANNEL}, cal);
        utils.copy(img, Views.hyperSlice(out_img, 3, 0));
        long sample = round(cal[2] / cal[0]);
        double[] scaling = {1, 1, sample};
        // Number of z-slices for isotropic XYZ sampling
        long zIso = img.dimension(dim_idx[2]) * sample;
        // Isotropic images containing spheres: sections of the sphere, Sphere1 distance to boundary, sphere2 distance to boundary
        // Img img_spheres = ArrayImgs.unsignedBytes(img.dimension(dim_idx[0]), img.dimension(dim_idx[1]), zIso, 3);
        // Create coordinates for isotropic image
        // force center to be on one slice and pixel of the isotropic image
        // convert um coordinates to pixel isotropic (integer)
        long[][] sphi = new long[2][4];
        for (int i = 0; i < sph.length; i++) {
            for (int j = 0; j < sph[0].length; j++)
                sphi[i][j] = round(sph[i][j]);
        }
        IJ.showProgress(0.3);
        Img<UnsignedByteType> img_spheres = ArrayImgs.unsignedBytes(dim[0], dim[1], zIso);
        createSpheres(img_spheres, sphi);
        // Perform distance transform for isotropic image
        IterableInterval bm1  = ij.op().threshold().apply(img_spheres, new UnsignedByteType(0));
        IterableInterval bm2  = ij.op().threshold().apply(img_spheres, new UnsignedByteType(1));
        bm1 = (IterableInterval) ij.op().run("math.subtract", bm1, bm2);
        final RandomAccessibleInterval<UnsignedByteType> DT1 = ij.op().image().distancetransform(ij.op().convert().bit(bm1), scaling);
        final RandomAccessibleInterval<UnsignedByteType> DT2 = ij.op().image().distancetransform(ij.op().convert().bit(bm2), scaling);
        final  Img<UnsignedByteType> DT1_int8 = (Img<UnsignedByteType>) ij.op().convert().int8((Img) DT1); // this converts the values of the image to 8bit
        final Img<UnsignedByteType> DT2_int8 = (Img<UnsignedByteType>) ij.op().convert().int8((Img) DT2); // this converts the values of the image to 8bit

        utils.copy(Views.subsample(DT1_int8, 1, 1, sample), Views.hyperSlice(out_img, 3, 0));
        utils.copy(Views.subsample(DT2_int8, 1, 1, sample), Views.hyperSlice(out_img, 3, 1));

        return out_img;



    }

    /**
     * @param img Image
     * @param sph Coordinate of 1-2 spheres given as array [id-sphere][cent_x, cent_y, cent_z, radius]
     * @param dirNuclei Vector of orientation along nuclei
     * @param dirMaxPol Vector of orientation with maximal polarity
     * @return An image with drawn spheres in it
     */
    private ImgPlus<UnsignedByteType> processImage (ImgPlus<UnsignedByteType> img, double[][] sph, double[][] dirNuclei, double[][] dirMaxPol) {
        int[] dim_idx = {img.dimensionIndex(Axes.X), img.dimensionIndex(Axes.Y), img.dimensionIndex(Axes.Z)};
        long[] dim = new long[img.numDimensions()];
        img.dimensions(dim);
        double[] cal = {img.averageScale(dim_idx[0]),
                img.averageScale(dim_idx[1]),
                img.averageScale(dim_idx[2])};
        // Contains DNA image, binary of spheres, distance transform of spheres

        ImgPlus<UnsignedByteType> out_img = new ImgPlus(ArrayImgs.unsignedBytes(dim[0], dim[1], dim[2], 3),
                FilenameUtils.getBaseName(img.getName()) + "_polarity.tif",
                new AxisType[] {Axes.X, Axes.Y, Axes.Z, Axes.CHANNEL}, cal);
        utils.copy(img, Views.hyperSlice(out_img, 3, 0));
        long sample = round(cal[2]/cal[0]);
        double[] scaling = {1, 1, sample};
        // Number of z-slices for isotropic XYZ sampling
        long zIso = img.dimension(dim_idx[2])*sample;
        // Isotropic images containing spheres: sections of the sphere, Sphere1 distance to boundary, sphere2 distance to boundary
        // Img img_spheres = ArrayImgs.unsignedBytes(img.dimension(dim_idx[0]), img.dimension(dim_idx[1]), zIso, 3);
        // Create coordinates for isotropic image
        // force center to be on one slice and pixel of the isotropic image
        // convert um coordinates to pixel isotropic (integer)
        long[][] sphi = new long[2][4];
        for(int i = 0; i < sph.length; i++) {
            for (int j = 0; j < sph[0].length; j++)
                sphi[i][j] = round(sph[i][j]);
        }
        IJ.showProgress(0.3);
        Img img_spheres = ArrayImgs.unsignedBytes(dim[0], dim[1], zIso);
        createSpheres(img_spheres, sphi);

        // direction from sphere1 to 2
        LinAlgHelpers.subtract(new double[] {sphi[1][0], sphi[1][1], sphi[1][2]},
                new double[] {sphi[0][0], sphi[0][1], sphi[0][2]}, dirNuclei[0]);
        LinAlgHelpers.normalize(dirNuclei[0]);
        LinAlgHelpers.scale(dirNuclei[0], -1, dirNuclei[1]);

        Img img_sections = ArrayImgs.unsignedBytes(dim[0], dim[1], dim[2]);
        utils.copy(Views.subsample(img_spheres,1, 1, sample), img_sections);

        Integer[][] sections = new Integer[2][nrsections];
        Integer[] sections12 = new Integer[nrsections*2];
        Arrays.setAll(sections[0], i -> i + 1);
        Arrays.setAll(sections[1], i -> i + nrsections + 1);
        Arrays.setAll(sections12, i -> i + 1);

        final ImgLabeling<Integer, UnsignedByteType> imgSphereLabels =
                ImgLabeling.fromImageAndLabels(Views.subsample(img_spheres, 1, 1, sample),
                        new ArrayList<Integer>(Arrays.asList(1, 2)));
        final LabelRegions< Integer > labelRegionsSpheres = new LabelRegions<>(imgSphereLabels);

        for (Integer idxNuc : labelRegionsSpheres.getExistingLabels()) {
            final LabelRegion<Integer> region = labelRegionsSpheres.getLabelRegion(idxNuc);
            createSphereSections(img_sections, region , sphi[idxNuc - 1], sections[idxNuc - 1], dirNuclei[idxNuc - 1], scaling);

            final double[] polarity = computePolarity(img, region, sphi[idxNuc - 1], dirNuclei[idxNuc - 1], scaling);
            IJ.log(String.format("Polarity along line: Nuc %d %.2f",  idxNuc, polarity[0]));
            IJ.log(String.format("Direction along line: Nuc  %d %.2f %.2f %.2f", idxNuc, dirNuclei[idxNuc-1][0],
                    dirNuclei[idxNuc-1][1], dirNuclei[idxNuc-1][2]));
        }


        utils.copy(img_sections, Views.hyperSlice(out_img, 3, 1));
        // Extracted nuclei
        final double[][] maxOrientation = new double[labelRegionsSpheres.numDimensions()][3];
        for (Integer idxNuc : labelRegionsSpheres.getExistingLabels()) {
            final LabelRegion<Integer> region = labelRegionsSpheres.getLabelRegion(idxNuc);
            final RandomAccessibleInterval<UnsignedByteType> nuc = clearOutsideRegion(img, region);
            final double[] sphere = new double[]{sphi[idxNuc - 1][0], sphi[idxNuc - 1][1], sphi[idxNuc - 1][2]};
            List<double[]> nucOrientations;
            if (allsigmas)
                nucOrientations = computeOrientations(nuc, sigmas_g_long, sphere, scaling);
            else
                nucOrientations = computeOrientations(nuc, sigmas_g_short, sphere, scaling);

            int idxOri = computeMaxPolarity(img, region, sphi[idxNuc - 1],
                    nucOrientations, scaling);
            dirMaxPol[idxNuc-1] = nucOrientations.get(idxOri);
            createSphereSections(img_sections, region, sphi[idxNuc - 1], sections[idxNuc - 1],
                    nucOrientations.get(idxOri), scaling);
            final double[] polarity = computePolarity(img, region, sphi[idxNuc - 1], nucOrientations.get(idxOri), scaling);
            IJ.log(String.format("Maximal polarity Nuc %d %.2f",  idxNuc, polarity[0]));
            maxOrientation[idxNuc-1] = nucOrientations.get(idxOri);
            IJ.log(String.format("Direction Nuc %d %.2f %.2f %.2f", idxNuc, maxOrientation[idxNuc-1][0],
                    maxOrientation[idxNuc-1][1], maxOrientation[idxNuc-1][2]));

        }
        utils.copy(img_sections, Views.hyperSlice(out_img, 3, 2));
        return out_img;
    }

    public static void main(String... args) throws Exception {
        ImageJ ij;
        ij = new ImageJ();
        ij.ui().showUI();
        //ij.command().run(drawSpheres.class, true, "file", "T:/Antonio (apoliti)/Projects_Facility/CavazzaTommaso/testdata/polarization/190504-cow18-01-t25/input_images_C04/Results.csv");

        //IJ.run("draw spheres", "file=");
    }
}
