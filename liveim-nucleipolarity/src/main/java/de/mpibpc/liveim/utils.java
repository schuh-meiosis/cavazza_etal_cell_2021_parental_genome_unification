package de.mpibpc.liveim;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import net.imglib2.*;
import net.imglib2.type.Type;
import net.imglib2.util.LinAlgHelpers;

import java.io.*;
import java.util.List;

public class utils {
    /**
     * Safe copy one image to another
     *
     * @param source source image
     * @param target target image
     * @param <T>    the class of pixel automatically determined during call (makes it generic)
     */
    static <T extends Type<T>> void copy(final RandomAccessible<T> source,
                                         final IterableInterval<T> target) {
        // create a cursor that automatically localizes itself on every move
        Cursor<T> targetCursor = target.localizingCursor();
        RandomAccess<T> sourceRandomAccess = source.randomAccess();

        // iterate over the input cursor
        while (targetCursor.hasNext()) {
            // move input cursor forward
            targetCursor.fwd();

            // set the output cursor to the position of the input cursor
            sourceRandomAccess.setPosition(targetCursor);

            // set the value of this pixel of the output image, every Type supports T.set( T type )
            targetCursor.get().set(sourceRandomAccess.get());
        }
    }

    /**
     * Read a comma separated file with fname X Y Z Radius coordinates of a sphere
     *
     * @param fname   filename where Coordinates of spheres are stored
     * @param spheres A list of coordinates that will be parsed
     * @return a message string
     * @throws Exception
     */
    static String readCsv(String fname, List<String[]> spheres)  {

        try  {
            CSVReader reader = new CSVReader(new FileReader(fname));
            String[] nextLine;

            while ((nextLine = reader.readNext()) != null) {
                spheres.add(nextLine);
            }

        } catch (IOError | IOException  e) {
            return ("Not able to read or found the file " + e.getMessage());
        }

        return ("read file" + fname);
    }

    static void writeDataAtOnce(File directions_file, String[] header,  List<String[]> data, Boolean append)  {
        // first create file object for file placed at location
        // specified by filepath
        //File file = new File(filePath);
        try {
            if (!directions_file.exists())
                append = false;
            FileWriter outputfile = new FileWriter(directions_file, append);
            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputfile);
            if (!append) {
                writer.writeNext(new String[]{"Units in pixels with XY pixel size as reference"});
                writer.writeNext(header);
            }
            writer.writeAll(data);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param point     Point in space
     * @param coef      Coefficients of the plane coef[0]*x + coef[1]*y + coef[2]*z + coef[3] = 0
     * @return          Distance of point to plane
     */
    static double distanceToPlane(Localizable point, double[] coef){
        double d;
        double[] dpoint = new double[point.numDimensions()];
        point.localize(dpoint);

        d = LinAlgHelpers.dot(new double[] {coef[0], coef[1], coef[2]}, dpoint) + coef[3];
        return d;
    }

    /**
     * Normal plane to 2 spheres where a pnt  on the plane fulfills both sphere equations
     * (pnt - cent1)^2 - rad1^2 = 0 and (pnt - cent2)^2 - rad2^2 = 0
     * 2*pnt*(cent2-cent1) - cent2^2 + cent1^2 - rad1^2 + rad2^2 = 0  (all vectors)
     * @param cnt1      center of sphere1 XYZ
     * @param cnt2      center of sphere2 XYZ
     * @param radius1   radius of sphere1
     * @param radius2   radius of sphere2
     * @return a double array [a, b, c, d] with a*x+b*y+c*z + d = 0 and |a^2+b^2+c^2| = 1.
     *         (a, b, c) are the coordinates of the normal vector
     */
    static double[] planeNormal(double[] cnt1, double[] cnt2, double radius1, double radius2){
        double[] norm = new double[cnt1.length];
        LinAlgHelpers.subtract(cnt2, cnt1, norm);
        double d;
        if (LinAlgHelpers.distance(cnt1, cnt2) < radius1 + radius2){ // spheres are crossing
            d = (LinAlgHelpers.squareLength(cnt1) - LinAlgHelpers.squareLength(cnt2) +
                    radius2 * radius2 - radius1 * radius1) / 2.0;
            d = d / LinAlgHelpers.length(norm);
            LinAlgHelpers.normalize(norm);
        } else {
            // return the plane given by the point in the middle of the 2 spheres, perpendicular to the 2 spheres.
            LinAlgHelpers.normalize(norm);
            double[] mid = {(cnt1[0]+cnt2[0])/2, (cnt1[1]+cnt2[1])/2, (cnt1[2]+cnt2[2])/2};
            d = - LinAlgHelpers.dot(mid, norm);
        }
        return new double[]{norm[0], norm[1], norm[2], d};
    }

    /**
     * Compute distance^2 between two points
     * @param point1 Coordinates of point1
     * @param point2 Coordinates of point2
     * @return A long of the distance^2
     */
    static long distanceSqLocalizable(Localizable point1, Localizable point2){
        long d = 0;
        for (int i = 0; i < point1.numDimensions(); i++){
            d +=  (point1.getLongPosition(i) - point2.getLongPosition(i))*(point1.getLongPosition(i) - point2.getLongPosition(i));
        }
        return d;
    }

    /**
     * @param sphere1 coordinates in um XYZR
     * @param sphere2 coordinates in um XYZR
     * @param cal XYZ pixelsize
     * @return {{sphere1_X, sphere1_Y, sphere1_Z, sphere1_R}, } scaled to XY pixels (isotropic)
     */
    static double[][] getSphereParameters(String[] sphere1, String[] sphere2, double[] cal) {
        // boneJ Z-center is with base 1. Transform to base 0 and scale
        double[][] sph = {
                {Double.parseDouble(sphere1[1])/cal[0],
                        Double.parseDouble(sphere1[2])/cal[0],
                        (Double.parseDouble(sphere1[3])/cal[2] - 1)*cal[2]/cal[0],
                        Double.parseDouble(sphere1[4])/cal[0]},
                {Double.parseDouble(sphere2[1])/cal[0],
                        Double.parseDouble(sphere2[2])/cal[0],
                        (Double.parseDouble(sphere2[3])/cal[2] - 1)*cal[2]/cal[0],
                        Double.parseDouble(sphere2[4])/cal[0]}};
        return sph;
    }
}