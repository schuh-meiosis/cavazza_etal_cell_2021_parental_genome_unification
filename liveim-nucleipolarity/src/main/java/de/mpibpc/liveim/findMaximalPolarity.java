package de.mpibpc.liveim;

import ij.IJ;
import net.imagej.Dataset;
import net.imagej.ImageJ;
import ij.ImagePlus;
import ij.ImageStack;
import net.imagej.ImgPlus;
import net.imagej.ops.OpService;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.RealLocalizable;
import net.imglib2.algorithm.binary.Thresholder;
import net.imglib2.algorithm.labeling.ConnectedComponentAnalysis;
import net.imglib2.img.Img;
import net.imglib2.algorithm.labeling.ConnectedComponents;
import net.imglib2.algorithm.labeling.ConnectedComponents.StructuringElement;
import net.imglib2.img.array.ArrayImgs;
import net.imglib2.img.display.imagej.ImageJFunctions;
import net.imglib2.ops.parse.token.Int;
import net.imglib2.roi.labeling.ImgLabeling;
import net.imglib2.roi.labeling.LabelRegion;
import net.imglib2.roi.labeling.LabelRegions;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import org.scijava.ItemIO;
import org.scijava.command.Command;
import org.scijava.convert.ConvertService;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;
import inra.ijpb.plugins.ExtendedMinAndMax3DPlugin;
import net.imglib2.algorithm.gauss.Gauss;
import org.scijava.ui.UIService;
import net.imglib2.type.logic.BitType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.imglib2.roi.Regions;

public class findMaximalPolarity<T extends RealType<T>> implements Command{

    @Parameter
    private Img image;

    @Parameter
    private ImageJ ij;

    @Parameter
    private OpService opService;

    @Parameter
    private UIService uiService;

    @Override
    public void run() {
         // define the sigma for each dimension
        final double[][] sigmas = {{8.0, 8.0, 0}, {16.0, 16.0, 0.0}, {16.0, 16.0, 1.0},
                {32.0, 32.0, 0.0}, {32.0, 32.0, 1.0}, {32.0, 32.0, 2.0}};
        List<RandomAccessibleInterval<T>> results = new ArrayList<>();

        //Create a series of gaussian filtered images and add them to results
        for (double [] sigma : sigmas)
            results.add(opService.filter().gauss(image, sigma));
        final long[] dims = new long[ image.numDimensions() ];
        image.dimensions(dims);
        final StructuringElement se = StructuringElement.FOUR_CONNECTED;
        final Iterator< String > labelCreator = new Iterator< String >()
        {
            int id = 0;

            @Override
            public boolean hasNext()
            {
                return true;
            }

            @Override
            public synchronized String next()
            {
                return "l" + ( id++ );
            }
        };
        // compute for each image extended maxima using MorphoLibJ.
        // This gives a set of possible coordinates where to compute the maximal polarity
        List<RealLocalizable>  directions = new ArrayList<RealLocalizable>();
        for (RandomAccessibleInterval<T> elem : results) {
            final Img<IntType> labelImg = ArrayImgs.ints( dims );

            final ImgLabeling< String, IntType > labeling = new ImgLabeling<>( labelImg );
            // this is a hack to use the MorphoLibJ library that only works with images in memory
            ImagePlus imp = ImageJFunctions.wrap(elem, "Gauss").duplicate();
            ImageStack stack = imp.getStack();
            ExtendedMinAndMax3DPlugin.Operation op = ExtendedMinAndMax3DPlugin.Operation.fromLabel("Extended Maxima");
            ImageStack result = op.apply(stack, 3, 6);
            ImagePlus impr = new ImagePlus("gauss", result);
            final Img <BitType> mask = ImageJFunctions.wrap(impr);
            ImageJFunctions.show(mask);
            ConnectedComponents.labelAllConnectedComponents(mask, labeling, labelCreator, se );
            ImageJFunctions.show( labelImg, "labelImg" );
            final LabelRegions< String > labelRegions = new LabelRegions<>( labeling );
            for (final LabelRegion< String > region : labelRegions){
                RealLocalizable center = region.getCenterOfMass();
                directions.add(region.getCenterOfMass());
                IJ.log("" + center.getDoublePosition(0) +  " " +
                        center.getDoublePosition(1) + " " + center.getDoublePosition(2)+ "\n");
            }
        }

        // Create regions along directions
        //Regions.sample(region, image).cursor();


        // For every image calculate extended maxima

        // need a stack for extended maxima
        // ImageStack stack = img.getStack();

        //
    }


    public static void main(String... args) throws Exception {
        ImageJ ij;
        ij = new ImageJ();
        ij.ui().showUI();
        final ImagePlus imp =  IJ.openImage("T:/Antonio (apoliti)/Projects_Facility/CavazzaTommaso/testdata/polarization/nuc1.tif");
        imp.show();
        /* final Img< UnsignedByteType > image = ImageJFunctions.wrap( imp );
        ImageJFunctions.show( image );
        final double[][] sigmas = {{8.0, 8.0, 0}};

        //Create a series of gaussian filtered images
        final Img< UnsignedByteType > gauss = (Img< UnsignedByteType >) ij.op().filter().gauss(image, sigmas[0]);

        final long[] dims = new long [image.numDimensions()];
        //final long[] dims = {450, 450, 25};
        image.dimensions( dims );
        final Img<IntType> labelImg = ArrayImgs.ints( dims );
        final ImgLabeling< String, IntType > labeling = new ImgLabeling<>( labelImg );
        final StructuringElement se = StructuringElement.FOUR_CONNECTED;
        final Iterator< String > labelCreator = new Iterator< String >()
        {
            int id = 0;

            @Override
            public boolean hasNext()
            {
                return true;
            }

            @Override
            public synchronized String next()
            {
                return "l" + ( id++ );
            }
        };
        ImagePlus imp2 = ImageJFunctions.wrap(gauss, "Gauss").duplicate();
        ImageStack stack = imp2.getStack();
        ExtendedMinAndMax3DPlugin.Operation op = ExtendedMinAndMax3DPlugin.Operation.fromLabel("Extended Maxima");
        ImageStack result = op.apply(stack, 3, 6);
        ImagePlus impr = new ImagePlus("gauss", result);
        final UnsignedByteType threshold = new UnsignedByteType( 200 );
        final Img< BitType > mask = Thresholder.threshold( ImageJFunctions.wrap(impr), threshold, true, 1 );
        ImageJFunctions.show(mask);

        ConnectedComponents.labelAllConnectedComponents(mask, labeling, labelCreator, se );
        ImageJFunctions.show( labelImg, "labelImg" );*/


    }
}