package de.mpibpc.liveim;

//import de.biomedical_imaging.ij.shapeSmoothing.ShapeSmoothingUtil;
import ij.IJ;
import ij.ImagePlus;
import net.imagej.DatasetService;
import net.imagej.ImageJ;
import net.imagej.ImgPlus;
import net.imagej.axis.Axes;
import net.imagej.axis.AxisType;
import net.imagej.ops.OpService;
import net.imagej.ops.special.computer.UnaryComputerOp;
import net.imglib2.*;
import net.imglib2.algorithm.labeling.ConnectedComponents;
import net.imglib2.algorithm.morphology.Opening;
import net.imglib2.algorithm.neighborhood.CenteredRectangleShape;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgs;
import net.imglib2.roi.labeling.ImgLabeling;
import net.imglib2.roi.labeling.LabelRegion;
import net.imglib2.roi.labeling.LabelRegions;
import net.imglib2.type.Type;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.util.Intervals;
import net.imglib2.view.Views;
import org.scijava.ItemIO;
import org.scijava.command.Command;
import org.scijava.convert.ConvertService;
import org.scijava.log.LogService;
import org.scijava.plugin.Parameter;

import java.util.Iterator;

//@Plugin(type = Command.class, menuPath = "Liveim > Process Binary")
public class processBinary implements Command {
    @Parameter
    private LogService logService;
    @Parameter
    private OpService opService;

    @Parameter
    private ConvertService convertService;

    @Parameter
    private DatasetService datasetService;

    @Parameter(type = ItemIO.INPUT, label = "Probability map")
    private Img prob;

    @Parameter(type = ItemIO.INPUT, label = "Fluorescence data")
    private ImgPlus fluo;

    @Parameter(type = ItemIO.INPUT, label = "Thrs. prob.", initializer = "2")
    private int thr;

    @Parameter(type = ItemIO.INPUT, label = "min. Area", initializer = "150")
    private int minarea;

    @Parameter(type = ItemIO.INPUT, label = "min. vol", initializer = "10000")
    private int minvol;

    private int open_size = 1; // this is an 11 sized square
    @Parameter
    private ImageJ ij;

    @Override
    public void run() {
        //binarize the image probability image
        double[] cal = {fluo.averageScale(fluo.dimensionIndex(Axes.X)), fluo.averageScale(fluo.dimensionIndex(Axes.Y)),
                fluo.averageScale(fluo.dimensionIndex(Axes.Z))};
        System.out.println(fluo.averageScale(fluo.dimensionIndex(Axes.Z)));

        Img<BitType> bin_src = (Img) opService.threshold().apply(prob, new UnsignedByteType(thr));
        Img<BitType> bin_open = ArrayImgs.bits(Intervals.dimensionsAsLongArray(bin_src));
        Img<BitType> bin_filterarea = ArrayImgs.bits(Intervals.dimensionsAsLongArray(bin_src));
        Img<BitType> bin_fill2D = ArrayImgs.bits(Intervals.dimensionsAsLongArray(bin_src));
        Img<BitType> bin_filtervolume = ArrayImgs.bits(Intervals.dimensionsAsLongArray(bin_src));

        // opening operation this removes
        CenteredRectangleShape strel = new CenteredRectangleShape(new int[]{open_size, open_size, 0}, true);
        bin_open = Opening.open(bin_src, strel, 12);
        // Perform 2D cca to remove further small objects
        analyzeParticles2D(bin_filterarea, bin_open, minarea);
        // fill holes 2D
        final UnaryComputerOp op = (UnaryComputerOp) opService.op("fillHoles", bin_filterarea);
        opService.slice(bin_fill2D, bin_filterarea, op, new int[]{0,1}); // which dimensions you would like to process -0:X, 1:Y


        // Filter CC by volume
        analyzeParticles3D(bin_filtervolume, bin_fill2D, minvol);
        // This does not maintain the axis order!!
        // ImagePlus imp1 = ImageJFunctions.wrap(imgPlus, "animage");
        // Need to pass by a dataset. Should not be like this
        ImagePlus imp = convertService.convert(datasetService.create(new ImgPlus(bin_filtervolume, "filtervolume",
                new AxisType[] {Axes.X, Axes.Y, Axes.Z}, cal)),
                ImagePlus.class);
        imp.show();
        IJ.save( imp, "/Users/apoliti/ownCloud/CavazzaTommaso/ProcessData/190406_zygote_cow15_8003_01_T21_bin.tif");
        //This does not work
        //        try {
        //            ij.io().save(datasetService.create(imgPlus), "/Users/apoliti/Desktop/test.tif");
        //        } catch( Exception e){
        //            ij.log().error("was not able to save the file");
        //        }
        //ij.ui().show("opened", bin_open);
        //ij.ui().show("filterArea", bin_filterarea);
        //ij.ui().show("fill2D", bin_fill2D);
        //ij.ui().show("filtervolume",imgPlus);




    }


    //Run 2D fillHoles as 3D operation is really slow
//        final Img<BitType> filled2D = (Img<BitType>) opService.run("create.img", imgbit);
//        final UnaryComputerOp op = (UnaryComputerOp) opService.op("fillHoles", imgbit);
//        opService.slice(filled2D, imgbit, op, new int[]{0,1}); // which dimensions you would like to process -0:X, 1:Y
//
//        // Show as ImgPlus which contains Axes
//        final ImgPlus imgPlus = new ImgPlus(filled2D, "filled2d", new AxisType[]{Axes.X, Axes.Y, Axes.Z, Axes.TIME});
//        ij.ui().show("filled2d", imgPlus);

    /**
     * Computes CC for each slice and filter by area.
     * @param source source binary
     * @param target image containing a binary of the filtered image
     * @param minarea minimal area
     */
    private  void analyzeParticles2D(final Img< BitType > target,
                                     final Img< BitType > source, long minarea){

        final Img <IntType> labelImg = ArrayImgs.ints(source.dimension(0), source.dimension(1));
        ImgLabeling<String, IntType> labeling = new ImgLabeling<>(labelImg);
        LabelRegions< String > labelRegions;

        for (int i = 0; i < source.dimension(2) ; i++) {
            final RandomAccessibleInterval<BitType> slice = Views.hyperSlice(source, 2, i);
            opService.labeling().cca(labeling, slice, ConnectedComponents.StructuringElement.EIGHT_CONNECTED);
            labelRegions = new LabelRegions<>(labeling);

            for (final LabelRegion<String> region : labelRegions) {
                if (region.size() >= minarea) {
                    final RandomAccess<BitType> access = Views.hyperSlice(target, 2, i).randomAccess();
                    final Cursor<Void> cursor = region.cursor();
                    while (cursor.hasNext()) {
                        cursor.fwd();
                        access.setPosition(cursor);
                        access.get().set(true);
                    }
                }
            }
        }
    }
    /**
     * Computes CC for and filter by volume
     * @param target image containing a binary of the filtered image
     * @param source source binary
     * @param minvol minimal volume
     */

    private  void analyzeParticles3D(final Img< BitType > target,
                                     final Img< BitType > source, long minvol) {
        final Iterator<String> labelCreator = new Iterator<String>(){
            int id = 0;

            @Override
            public boolean hasNext() { return true; }

            @Override
            public String next() {  return "l" + (id++); }
        };
        final Img<IntType> labelImg = ArrayImgs.ints(Intervals.dimensionsAsLongArray(source));
        ImgLabeling<String, IntType> labeling = new ImgLabeling<>(labelImg);
        opService.labeling().cca(labeling, source, ConnectedComponents.StructuringElement.EIGHT_CONNECTED, labelCreator);
        LabelRegions<String> labelRegions = new LabelRegions<>(labeling);
        final RandomAccess<BitType> access = target.randomAccess();
        for (final LabelRegion<String> region : labelRegions) {
            if (region.size() >= minvol) {
                final Cursor<Void> cursor = region.cursor();
                while (cursor.hasNext()) {
                    cursor.fwd();
                    access.setPosition(cursor);
                    access.get().set(true);
                }

            }
        }
    }
    /**
     * Copy from a source that is just RandomAccessible to an IterableInterval. Latter one defines
     * size and location of the copy operation. It will query the same pixel locations of the
     * IterableInterval in the RandomAccessible. It is up to the developer to ensure that these
     * coordinates match.
     *
     * Note that both, input and output could be Views, Img or anything that implements
     * those interfaces.
     *
     * @param source - a RandomAccess as source that can be infinite
     * @param target - an IterableInterval as target
     */
    public < T extends Type< T >> void copy(final RandomAccessible< T > source,
                                            final IterableInterval< T > target )
    {
        // create a cursor that automatically localizes itself on every move
        Cursor< T > targetCursor = target.localizingCursor();
        RandomAccess< T > sourceRandomAccess = source.randomAccess();

        // iterate over the input cursor
        while ( targetCursor.hasNext())
        {
            // move input cursor forward
            targetCursor.fwd();

            // set the output cursor to the position of the input cursor
            sourceRandomAccess.setPosition( targetCursor );

            // set the value of this pixel of the output image, every Type supports T.set( T type )
            targetCursor.get().set( sourceRandomAccess.get() );
        }
    }

    public static void main(String... args) throws Exception{
        ImageJ ij;
        ij  = new ImageJ();
        ij.ui().showUI();

        Object prob = ij.io().open("/Users/apoliti/ownCloud/CavazzaTommaso/ProcessData/190406_zygote_cow15_8003_01_T21.tif_foreground.tif");
        Object fluo = ij.io().open("/Users/apoliti/ownCloud/CavazzaTommaso/ProcessData/190406_zygote_cow15_8003_01_T21.tif");

        ij.ui().show(prob);
        ij.ui().show(fluo);
        //ij.command().run(processBinary.class, true, "prob", prob, "fluo", fluo, "thr", 2, "minsize2D", 300);
        ij.command().run(processBinary.class, true, "prob", prob, "fluo", fluo, "minarea", 150, "thr", 2, "minvol", 10000);
    }
}
