# Compute nuclear chromatin distribution in zygotes

The code is related to the article


>  Parental genome unification is highly error-prone in mammalian embryos. 
>
>  Tommaso Cavazza, Yuko Takeda, Antonio Z Politi, Magomet Aushev, Patrick Aldag, Clara Baker, Meenakshi Choudhary, Jonas Bucevičius, Gražvydas Lukinavičius, Kay Elder, Martyn Blayney, Andrea Lucas-Hahn, Heiner Niemann, Mary Herbert, Melina Schuh
>
>  [Cell 2021, in press](https://www.sciencedirect.com/science/article/pii/S009286742100492X?via%3Dihub)

## Workflow

The workflow computes metrics of the chromatin distribution in the two nuclei of a zygote before nuclear envelope breakdown. The metrics are 

- *Inner/outer fractions*: Quantify chromatin intensities in oriented sections of the nucleus. The sections are oriented in the direction of the 2 nuclei.
- *Maximal polarity*: Find an orientation along each nuclei that maximizes the chromatin fraction in one of the sections.
- *Nuclear occupancy*: Quantifies the amount of chromatin with respect to the total volume of the nucleus.
- *Surface occupancy*: Quantifies chromatin intensity in a region close to the surface with respect to inside of the nucleus.

The workflow uses different software and plugins. In short, starting from a *.czi* file the user has to 

1. Prepare data for processing (IJ script *SplitTpointsH2bchannel.ijm*)
2. Manually specify nuclei masks using spheres (IJ script *BoneJFitSphere.ijm*)
3. Compute the maximal polarity (IJ plugin *nuclei polarity*)
4. Segment and compute the distribution of the chromatin mass (Matlab code *computePoiDistribution.m*)_

## Installation
The imageJ plugin and scripts are installed when enabling the the update site 
https://sites.imagej.net/Liveim/, see [ImageJ update site](https://imagej.net/Following_an_update_site). 
Alternatively you can copy the jar file (``Repository > Tags`` ) to the *plugins* folder of ImageJ/Fiji. The plugin and scripts appear in the main menu
``Liveim > Cavazza-etal-2021``.  

For the identification of the nuclei boundaries enable the BoneJ update site **Help > Update > Manage Update sites> [x] BoneJ**  

Change I/O options to save column header but not row index, **Edit > Options > I/O Optons > [ ] Save row numbers** 

The matlab code (MATLAB2020a) requires bioformat, download and extract the [bioformat tools for MATLAB 6.3.0](https://docs.openmicroscopy.org/bio-formats/6.3.0/users/matlab/index.html)  in the *Documents/MATLAB/directory*.


## Data preparation

The workflow requires a folder for each movie file (format *czi*). 
First run the script *SplitTpointsH2bchannel.ijm* to create a file for the chromatin channel per time point. 
The script saves the tif stacks in a subdirectory (here channel 4 contains the H2B signal)
````
    - foldername_as_filename
        filename.czi
        - input_images_C04 (created by  SplitTpointsH2bchannel.ijm)
          filename_C04_T01.tif
          filename_C04_T02.tif 
          ...
````


## Nuclei masks and sphere sections

<figure>
<img src="./doc/workflow_v0.2_nuclei.png" alt="nuclei_workflow" width="400">
<figcaption>Figure 1: Schematic of the image processing workflow for computing the nuclei masks and sphere sections using 
the H2B signal. Created from a graphviz file  dot -Tpng file.gv -o file.png
</figcaption>
</figure>


<figure>
<img src="./doc/sphere_definition.jpg" alt="sphere_definition" width="600">
<img src="./doc/sphere_sections.jpg" alt="sections"  width = "300">
<figcaption>Figure 2: Manual segmentation of nuclei using spheres and labelled region to compute polarity measures. </figcaption>
</figure>

Nuclei have a nearly spherical structure, thus we fit to each nuclei a sphere given some reference points on at least 2 planes. 
We use [BoneJ plugin](https://imagej.net/BoneJ2) sphere fit. 
Note that BoneJ plugin works only on a single channel 3D image, and uses base 1 for the Z-coordinates. 


1. The user opens a single time point containing the chromatin signal
2. Using the point ROI tool of imageJ the user places points on the surface of one of the nuclei (at least on 2 planes). 
Execute the script *BoneJFitSphere.ijm*  to call the BoneJ plugin and save the ROI.
3. Repeat step 2 for the 2nd nucleus.
4. Repeat 1. to 3. for each time point and take care to always start with the same parental nucleus.
5. Save the results table as *Results.csv* in the same folder containing the single time points tif. 
6. Run the *nuclei polarity* plugin by specifying the *.csv* file.  

The plugin *nuclei polarity* creates two spherical masks using the sphere parameters and an axis along the two nuclei.  
The planes perpendicular to this axis subdivide the nucleus in different sections. 
For an ideal sphere a equal section height gives sphere sections with equal outer area $`A = 2*\pi*r*h`$, where $`h`$ is the height of the section and $`r`$ the radius of the sphere. 
The plugin returns a labelled image of the corresponding sections. The plugin also estimate the direction of maximal polarity. For this gaussian-filtered images with increasing 
filter size are computed. For every image the direction to the extended maxima is calculated. The direction that gives the highest polarity is used as an estimate. 
Finally a distance transform is also computed. 

Per image *nuclei polarity* returns up to 2 images:

* *filename_polarity.tif*: XYZC image with 3 channels. First channel is the chromatin intensity, second and
third channels are the labelled image along the nuclei direction and the maximal polarities respectively. 
* *filename_DT.tif*: XYZC image with 2 channels. Each channel correspond to the distance tranform for each nucleus. 
* *filename_directions.csv*: Contains the coordinates of the sphere, direction along the nuclei and the maximal direction.

Some of the options are: 
* *Maximal number of gauss-steps (slow)*: Either 1 or 7 different gauss-filters.
* *extendedMaxima(slow)*: Directly compute the center of gravity or first compute extended maxima and then the center of gravity
of each extended maxima region. 


The R-script *createIJbatchscripts.R* can be used to create IJ1 scripts to process the complete data set. 

## MATLAB scripts to compute the chromatin metrics 
The chromatin metrics is computed in the matlab code ``computePoiDistribution.m``. 
Run the code and specify the csv files generated by the imageJ plugin. 

### Identify the chromatin signal

<figure>
<img src="./doc/workflow_v0.2_chromatin.png" alt="chromatin_workflow" width="300">
<figcaption>Figure 3: Workflow to calculate the fluorescence signal on chromosomes </figcaption>
</figure>

<figure>
<img src="./doc/190406_zygote_cow15_8003_01_C02_twoH2B.gif" alt="chromatin_preseg" width="300">
<img src="./doc/190406_zygote_cow15_8003_01_C02_twoH2B_seg.gif" alt="chromatin_seg" width=300">
<figcaption>Figure 4: Chromatin signal left and the segmented mask overlay colored according to the nucleus ID (downsampled!)</figcaption>
</figure>

The H2B signal has a non-negligible fraction that is not on DNA. We thus performed a segmentation of the chromatin signal to remove the non-specific signal. 
In order to ensure that over time we do not undersegment the chromatin we performed an adaptive estimation of the threshold.
The threshold is set so that the *total intensity  - background* at any time point is within 10% of the value obtained at pre-NEBD. 

In short, the H2B signal is filtered using a 2D median and a 2D gaussian filter, this is performed for the whole image. Then for each nuclei we calculate the total intensity. 
This is a correction factor to account for bleaching or changes in fluorescence due to nuclei movement. A threshold *L* is automatically determined for the point of pre-NEBD. 
The total intensity in the area *>L* is computed, the average intensity in the area below threshold gives a background value. For each time point prior preNEBD, a value of 
the threshold is computed so to be within 10% of the preNEBD value. 



### Inner/outer polarity and maximal polarity
<figure>
<img src="./doc/workflow_v0.2_merged.png" alt="chromatin_workflow" width="600">
<figcaption>Figure 5: Chromatin metrics </figcaption>
</figure>

Polarity metrics are calculated from the mean intensities in the labelled regions normalized to the total 
mean intensities. 


### Nuclear occupancy
A measure of chromatin compaction is given by the number of thresholded pixels (~ Volume) normalized to the total number of pixels 
of the nucleus (~ total Volume). 

### Surface occupancy
The distance transform is used to subdivide the nuclei in a region close to the surface and a region inside the nucleus. 
For an exact sphere equal volume is achieved at an inner radius of $`r_i = 2^{-1/3} r \approx 0.8 r`$, where *r* is the radius of the nucleus. 
To maintain an algorithm independent of the geometry we compute the median distance for all pixels and use this to subdivide the nucleus,
this corresponds to equal volume of inner and outer portions. We use the fraction of intensity of the region close to the surface 
with respect to the total intensity as the surface occupancy. 





